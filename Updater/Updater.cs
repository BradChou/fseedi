﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Windows.Forms;

namespace Updater
{
    public partial class Updater : Form
    {
        public string parameter;

        public Updater(string parameter)
        {
            InitializeComponent();
            lbProcessing.Show();
            lbProcessing.Refresh();
            this.parameter = parameter;            
        }

        private void Updater_Shown(object sender, EventArgs e)
        {
            CheckVersion();
        }

        private void StartUploadSystem()
        {
            //執行主程式
            string szExeFile = ConfigurationManager.AppSettings["exe"];

            string exeFullPath = Path.Combine(Application.StartupPath, szExeFile);

            System.Diagnostics.Process.Start(exeFullPath, parameter);

            Application.Exit();
        }

        private void CheckVersion()
        {
            string path = ConfigurationManager.AppSettings["S3VersionTxtPath"];

            WebClient client = new WebClient();
            Stream stream = client.OpenRead(path);
            StreamReader reader = new StreamReader(stream);
            string versionStrOnline = reader.ReadToEnd();

            string currentVersionFile = Path.Combine(Application.StartupPath, "version.txt");

            FileInfo fi = new FileInfo(currentVersionFile);

            if (fi.Exists)
            {
                //檢查版本是否一致
                FileStream fs = new FileStream(currentVersionFile, FileMode.Open);

                StreamReader sr = new StreamReader(fs);

                string szCurrentVersion = sr.ReadToEnd();

                sr.Close();
                fs.Close();

                //版本一致時
                if (szCurrentVersion == versionStrOnline)
                {
                    //開啟印標程式
                    StartUploadSystem();
                }
                else
                {
                    //不一致時抓取資料
                    fi.Delete();
                    string szGetFile = ConfigurationManager.AppSettings["NewZip"];
                    GetNewVersion(szGetFile);
                    StartUploadSystem();
                }
            }
            else
            {
                //第一次使用
                string szGetFile = ConfigurationManager.AppSettings["NewZip"];
                GetNewVersion(szGetFile);
                StartUploadSystem();
            }
        }

        private void GetNewVersion(string szCurrentVersionFile)
        {
            string url = "https://storage-for-station.s3.us-east-2.amazonaws.com/FSEEDI/new_EDI/new_EDI.zip";

            WebClient client = new WebClient();
            client.DownloadFile(url, "new_EDI.zip");

            UnZipFile(Path.Combine(Application.StartupPath, szCurrentVersionFile));

            FileInfo fi = new FileInfo(szCurrentVersionFile);
            fi.Delete();

            client.Dispose();
        }

        private void UnZipFile(string szFileName)
        {
            try
            {
                ZipFile.ExtractToDirectory(szFileName, "new_EDI");
            }
            catch (Exception)
            {}

            //using (ZipInputStream s = new ZipInputStream(File.OpenRead(szFileName)))
            //{
            //    ZipEntry theEntry;
            //    while ((theEntry = s.GetNextEntry()) != null)
            //    {
            //        try
            //        {
            //            string fileName = Path.Combine("new_EDI", Path.GetFileName(theEntry.Name));

            //            if (fileName != String.Empty)
            //            {
            //                using (FileStream streamWriter = File.Create(fileName))
            //                {
            //                    int size = 2048;
            //                    byte[] data = new byte[2048];
            //                    while (true)
            //                    {
            //                        size = s.Read(data, 0, data.Length);
            //                        if (size > 0)
            //                        {
            //                            streamWriter.Write(data, 0, size);
            //                        }
            //                        else
            //                        {
            //                            break;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        catch
            //        {
            //            continue;
            //        }
            //    }
            //}
        }
    }
}
