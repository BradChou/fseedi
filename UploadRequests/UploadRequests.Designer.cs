﻿namespace UploadRequests
{
    partial class UploadRequests
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbImport = new System.Windows.Forms.Label();
            this.btnChooseFile = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lbProgress = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.pbUpload = new System.Windows.Forms.ProgressBar();
            this.btnPrint = new System.Windows.Forms.Button();
            this.rbA4 = new System.Windows.Forms.RadioButton();
            this.rbRoller = new System.Windows.Forms.RadioButton();
            this.rb2Size = new System.Windows.Forms.RadioButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.lbPrinter = new System.Windows.Forms.Label();
            this.txtPrinter = new System.Windows.Forms.TextBox();
            this.btnSetPrinter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbImport
            // 
            this.lbImport.AutoSize = true;
            this.lbImport.Location = new System.Drawing.Point(24, 42);
            this.lbImport.Name = "lbImport";
            this.lbImport.Size = new System.Drawing.Size(60, 15);
            this.lbImport.TabIndex = 0;
            this.lbImport.Text = "匯入excel";
            // 
            // btnChooseFile
            // 
            this.btnChooseFile.Location = new System.Drawing.Point(425, 34);
            this.btnChooseFile.Name = "btnChooseFile";
            this.btnChooseFile.Size = new System.Drawing.Size(75, 23);
            this.btnChooseFile.TabIndex = 1;
            this.btnChooseFile.Text = "選擇檔案";
            this.btnChooseFile.UseVisualStyleBackColor = true;
            this.btnChooseFile.Click += new System.EventHandler(this.btnChooseFile_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(101, 35);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(309, 23);
            this.txtFileName.TabIndex = 2;
            // 
            // lbProgress
            // 
            this.lbProgress.AutoSize = true;
            this.lbProgress.Location = new System.Drawing.Point(53, 173);
            this.lbProgress.Name = "lbProgress";
            this.lbProgress.Size = new System.Drawing.Size(31, 15);
            this.lbProgress.TabIndex = 3;
            this.lbProgress.Text = "訊息";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(101, 79);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "開始打單";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtProgress
            // 
            this.txtProgress.Location = new System.Drawing.Point(101, 173);
            this.txtProgress.Multiline = true;
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProgress.Size = new System.Drawing.Size(399, 110);
            this.txtProgress.TabIndex = 5;
            // 
            // pbUpload
            // 
            this.pbUpload.Location = new System.Drawing.Point(101, 121);
            this.pbUpload.Name = "pbUpload";
            this.pbUpload.Size = new System.Drawing.Size(399, 23);
            this.pbUpload.TabIndex = 6;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(280, 376);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "列印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // rbA4
            // 
            this.rbA4.AutoSize = true;
            this.rbA4.Location = new System.Drawing.Point(103, 307);
            this.rbA4.Name = "rbA4";
            this.rbA4.Size = new System.Drawing.Size(139, 19);
            this.rbA4.TabIndex = 9;
            this.rbA4.TabStop = true;
            this.rbA4.Text = "A4(一式6筆託運標籤)";
            this.rbA4.UseVisualStyleBackColor = true;
            // 
            // rbRoller
            // 
            this.rbRoller.AutoSize = true;
            this.rbRoller.Location = new System.Drawing.Point(103, 342);
            this.rbRoller.Name = "rbRoller";
            this.rbRoller.Size = new System.Drawing.Size(73, 19);
            this.rbRoller.TabIndex = 10;
            this.rbRoller.TabStop = true;
            this.rbRoller.Text = "捲筒列印";
            this.rbRoller.UseVisualStyleBackColor = true;
            // 
            // rb2Size
            // 
            this.rb2Size.AutoSize = true;
            this.rb2Size.Location = new System.Drawing.Point(103, 380);
            this.rb2Size.Name = "rb2Size";
            this.rb2Size.Size = new System.Drawing.Size(73, 19);
            this.rb2Size.TabIndex = 11;
            this.rb2Size.TabStop = true;
            this.rb2Size.Text = "一筆兩式";
            this.rb2Size.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(196, 79);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(95, 23);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "清空列印資料";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lbPrinter
            // 
            this.lbPrinter.AutoSize = true;
            this.lbPrinter.Location = new System.Drawing.Point(280, 309);
            this.lbPrinter.Name = "lbPrinter";
            this.lbPrinter.Size = new System.Drawing.Size(43, 15);
            this.lbPrinter.TabIndex = 13;
            this.lbPrinter.Text = "印表機";
            // 
            // txtPrinter
            // 
            this.txtPrinter.Location = new System.Drawing.Point(280, 327);
            this.txtPrinter.Name = "txtPrinter";
            this.txtPrinter.Size = new System.Drawing.Size(220, 23);
            this.txtPrinter.TabIndex = 14;
            // 
            // btnSetPrinter
            // 
            this.btnSetPrinter.Location = new System.Drawing.Point(425, 376);
            this.btnSetPrinter.Name = "btnSetPrinter";
            this.btnSetPrinter.Size = new System.Drawing.Size(75, 23);
            this.btnSetPrinter.TabIndex = 15;
            this.btnSetPrinter.Text = "設定印表機";
            this.btnSetPrinter.UseVisualStyleBackColor = true;
            this.btnSetPrinter.Click += new System.EventHandler(this.btnSetPrinter_Click);
            // 
            // UploadRequests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 432);
            this.Controls.Add(this.btnSetPrinter);
            this.Controls.Add(this.txtPrinter);
            this.Controls.Add(this.lbPrinter);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.rb2Size);
            this.Controls.Add(this.rbRoller);
            this.Controls.Add(this.rbA4);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pbUpload);
            this.Controls.Add(this.txtProgress);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lbProgress);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.btnChooseFile);
            this.Controls.Add(this.lbImport);
            this.Name = "UploadRequests";
            this.Text = "小幫手";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbImport;
        private System.Windows.Forms.Button btnChooseFile;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label lbProgress;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtProgress;
        private System.Windows.Forms.ProgressBar pbUpload;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.RadioButton rbA4;
        private System.Windows.Forms.RadioButton rbRoller;
        private System.Windows.Forms.RadioButton rb2Size;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lbPrinter;
        private System.Windows.Forms.TextBox txtPrinter;
        private System.Windows.Forms.Button btnSetPrinter;
    }
}

