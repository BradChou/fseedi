﻿using Common;
using ExcelDataReader;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using UploadRequests.Model;

namespace UploadRequests
{
    public partial class UploadRequests : Form
    {
        UserEntity User { get; set; }

        DataTable dataTable;

        List<string> checkNumbers = new List<string>();

        string printerName = string.Empty;

        public UploadRequests(string parameter)
        {
            InitializeComponent();

            parameter = WebUtility.UrlDecode(parameter);
            User = JsonConvert.DeserializeObject<UserEntity>(parameter);

            GetCheckNumberInterval(User.CustomerCode);
            GetSendCustomerInfo(User.CustomerCode);
            GetPrinterSetting();
        }

        public void GetCheckNumberInterval(string customerCode)
        {
            string getDeliveryNumberSettingByCustomer =
                string.Format($"select top 1 begin_number, end_number, current_number from tbDeliveryNumberSetting where customer_code = '{customerCode}' AND IsActive = 1");
            DataTable customerCheckNumRange = DbAdaptor.GetDataTableBySql(getDeliveryNumberSettingByCustomer);

            if (customerCheckNumRange.Rows.Count == 0)
            {
                string getDeliveryOnceSettingByCustomer =
                   string.Format($"select top 1 begin_number, end_number, current_number from tbDeliveryNumberOnceSetting where customer_code = '{customerCode}' AND IsActive = 1");
                customerCheckNumRange = DbAdaptor.GetDataTableBySql(getDeliveryNumberSettingByCustomer);

                if (customerCheckNumRange.Rows.Count == 0)
                {
                    MessageBox.Show("查無託運單貨號區間與預購袋區間，請冾系統管理員。");
                    return;
                }
                else
                {
                    User.IsPreOrderBagUser = true;
                }
            }

            User.CheckNumStart = customerCheckNumRange.Rows[0].Field<long>("begin_number");
            User.CheckNumEnd = customerCheckNumRange.Rows[0].Field<long>("end_number");
            User.CurrentCheckNum = customerCheckNumRange.Rows[0].Field<long>("current_number");

        }

        private void AddCurrentCheckNum()
        {
            if (User.IsPreOrderBagUser)
            {
                if (User.CurrentCheckNum == 0)
                {
                    User.CurrentCheckNum = User.CheckNumStart;
                }
                else if (User.CurrentCheckNum == User.CheckNumEnd)
                {
                    MessageBox.Show("預購袋使用完畢，請冾系統管理員。");
                    System.Environment.Exit(0);
                }
                else
                {
                    User.CurrentCheckNum = User.CurrentCheckNum + 1;
                    UpdateCheckNumberOnceSetting();
                }
            }
            else
            {
                if (User.CurrentCheckNum == 0 || User.CurrentCheckNum == User.CheckNumEnd)
                {
                    User.CurrentCheckNum = User.CheckNumStart;
                }
                else
                {
                    User.CurrentCheckNum = User.CurrentCheckNum + 1;
                    UpdateCheckNumberSetting();
                }
            }
        }

        private void GetSendCustomerInfo(string customerCode)
        {
            string getCustomerInfo =
                string.Format($@"select top 1 customer_name, telephone, supplier_code, shipments_city + shipments_area + shipments_road as 'senderAddress'
                        from tbCustomers where customer_code = '{customerCode}'");

            DataTable customerInfo = DbAdaptor.GetDataTableBySql(getCustomerInfo);


            User.SendContact = customerInfo.Rows[0].Field<string>("customer_name");
            User.SendAddress = customerInfo.Rows[0].Field<string>("senderAddress");
            User.SendTel = customerInfo.Rows[0].Field<string>("telephone");
            User.SupplierCode = customerInfo.Rows[0].Field<string>("supplier_code");
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm|csv|*.csv"
            })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFileName.Text = ofd.FileName;

                    try
                    {
                        using (var stream = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                    {
                                        UseHeaderRow = true
                                    }
                                });

                                dataTable = result.Tables[0];
                            }
                        }
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("檔案開啟失敗，請確認沒有其他程式正在使用該檔案");
                    }
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            List<RequestFieldEntity> entities = new List<RequestFieldEntity>();

            txtProgress.Text += "\r\n開始匯入 excel ";
            Application.DoEvents();

            for (int i = 2; i < dataTable.Rows.Count; i++)
            {
                DataRow row = dataTable.Rows[i];

                if (IsEmpty(row))
                    break;

                #region 從excel取值
                string orderNumber = row[0].ToString();
                string receiveName = row[1].ToString();
                string receiveTel1 = row[2].ToString();
                string receiveTel2 = row[3].ToString();
                string receiveAddress = row[4].ToString();
                string peices = row[5].ToString();
                string weight = row[6].ToString();
                string holiday = row[7].ToString();
                string roundTrip = row[8].ToString();
                string returnReceipt = row[9].ToString();
                string collectionMoney = row[10].ToString();
                int collectionMoneyNum;
                string infoiceDesc = row[11].ToString();
                string assignDeliveryDate = row[12].ToString();
                string assignDeliveryTime = row[13].ToString();

                string articleNumber = row[14].ToString();
                string sendPlatform = row[15].ToString();
                string articleName = row[16].ToString();
                string bagno = row[17].ToString();
                //string checkNumber = row[18].ToString();

                string subpoenaCategory = "11";

                if (int.TryParse(collectionMoney, out collectionMoneyNum))
                {
                    if (collectionMoneyNum > 0)
                        subpoenaCategory = "41";
                }

                // 必填欄位
                if (receiveName.Length == 0 || receiveAddress.Length == 0 || receiveTel1.Length == 0 || peices.Length == 0)
                {
                    txtProgress.Text += $"\r\n第{i}行缺少必填欄位";
                    Application.DoEvents();
                    continue;
                }

                RequestFieldEntity request = new RequestFieldEntity
                {
                    LineNumber = i,
                    //checknumber = checkNumber,
                    ordernumber = orderNumber,
                    receiptflag = returnReceipt,
                    receivecontact = receiveName,
                    ReceiverAddress = receiveAddress,
                    receivetel1 = receiveTel1,
                    receivetel2 = receiveTel2,
                    plates = peices,
                    subpoenacategory = subpoenaCategory,
                    collectionmoney = collectionMoney,
                    arriveassigndate = assignDeliveryDate,
                    timeperiod = assignDeliveryTime,
                    SendPlatform = sendPlatform,
                    ArticleName = articleName,
                    ArticleNumber = articleNumber,
                    printdate = DateTime.Now.ToString(),
                    roundtrip = roundTrip,
                    CbmSize = "1",
                    invoicedesc = infoiceDesc,
                    receivecustomercode = "",
                    sendcontact = User.SendContact,
                    sendtel = User.SendTel,
                    SenderAddress = User.SendAddress,
                    customercode = User.CustomerCode,
                    suppliercode = User.SupplierCode
                };
                entities.Add(request);
                #endregion
            }

            txtProgress.Text += Environment.NewLine + "excel 讀取完成";

            CallApiByEntities(entities);
        }

        private void CallApiByEntities(List<RequestFieldEntity> requestFieldEntities)
        {
            List<RequestFieldEntity> failedRecord = new List<RequestFieldEntity>();

            txtProgress.Text += "\r\n開始打單";
            Application.DoEvents();

            Thread backgroundThread = new Thread(
               new ThreadStart(() =>
               {
                   for (int n = 0; n < requestFieldEntities.Count; n++)
                   {
                       AddCurrentCheckNum();
                       requestFieldEntities[n].checknumber = User.CurrentCheckNum.ToString();
                       RequestFieldEntity[] requests = { requestFieldEntities[n] };

                       string queryString = JsonConvert.SerializeObject(requests);

                       try
                       {
                           string result = CallApi(User.Token, queryString);
                           JObject jObject = JsonConvert.DeserializeObject(result) as JObject;
                           if (jObject.Value<bool>("resultcode") == true)
                           {
                               checkNumbers.Add(requestFieldEntities[n].checknumber.ToString());
                           }
                           else
                           {
                               if (jObject.Value<string>("resultdesc").Contains("簽章"))
                               {
                                   string psw = "password";
                                   if (InputBox("重新登入", "您的連線已經逾時，請重新輸入密碼：", ref psw) == DialogResult.OK)
                                   {
                                       User.Token = LoginMethod.GetToken(User.CustomerCode, psw);
                                   }
                                   else
                                   {                                       
                                       Environment.Exit(Environment.ExitCode);
                                   }
                               }

                               txtProgress.BeginInvoke(
                                    new Action(() =>
                                    {
                                        txtProgress.Text += Environment.NewLine + $"第{n + 2}行打單失敗";
                                    }
                                ));

                               failedRecord.Add(requestFieldEntities[n]);
                           }
                       }
                       catch (Exception ex)
                       {
                           txtProgress.BeginInvoke(
                                new Action(() =>
                                {
                                    txtProgress.Text += "\r\n" + ex.Message;
                                }
                           ));

                           MessageBox.Show("錯誤");
                           Environment.Exit(Environment.ExitCode);
                       }

                       pbUpload.BeginInvoke(
                           new Action(() =>
                           {
                               int p = (n + 1) * 100 / requestFieldEntities.Count;
                               pbUpload.Value = p > 100 ? 100 : p;
                           }
                       ));
                   }

                   // 打失敗的重來
                   if (failedRecord.Count > 0)
                   {
                       pbUpload.BeginInvoke(
                               new Action(() =>
                               {
                                   pbUpload.Value = 0;
                               }
                           ));
                       txtProgress.BeginInvoke(
                               new Action(() =>
                               {
                                   txtProgress.Text += Environment.NewLine + "重打失敗紀錄";
                               }
                           ));
                       int retryTimes = int.Parse(ConfigurationManager.AppSettings["RetryTimes"]);
                       List<RequestFieldEntity> failedRecordCopy = failedRecord.ToList();
                       for (int j = 0; j < failedRecordCopy.Count; j++)
                       {
                           for (int i = 0; i < retryTimes; i++)
                           {
                               RequestFieldEntity[] requests = { failedRecordCopy[j] };

                               string queryString = JsonConvert.SerializeObject(requests);

                               string result = CallApi(User.Token, queryString);

                               JObject jObject = JsonConvert.DeserializeObject(result) as JObject;
                               if (jObject.Value<bool>("resultcode") == true)
                               {
                                   checkNumbers.Add(failedRecordCopy[j].checknumber.ToString());
                                   failedRecord.RemoveAt(j);
                                   break;
                               }
                               else
                               { }
                           }
                           pbUpload.BeginInvoke(
                               new Action(() =>
                               {
                                   pbUpload.Value = j / failedRecord.Count * 100;
                               }
                           ));
                       }
                   }

                   string finalMessage = $"\r\n打單程序完成，共有{checkNumbers.Count}筆成功，\r\n共有{failedRecord.Count}筆失敗：\r\n";

                   foreach (var e in failedRecord)
                   {
                       finalMessage += $"第{e.LineNumber}行, ";
                   }

                   txtProgress.BeginInvoke(
                        new Action(() =>
                        {
                            txtProgress.Text += finalMessage;
                        }
                    ));
               }
            ));
            backgroundThread.Start();
        }

        private void UpdateCheckNumberSetting()
        {
            string updateQuery = $"update tbDeliveryNumberSetting set current_number = {User.CurrentCheckNum} where customer_code = '{User.CustomerCode}'";

            DbAdaptor.ExecuteNonQuery(updateQuery);
        }

        private void UpdateCheckNumberOnceSetting()
        {
            string updateQuery = $"update tbDeliveryNumberOnceSetting set current_number = {User.CurrentCheckNum} where customer_code = '{User.CustomerCode}'";

            DbAdaptor.ExecuteNonQuery(updateQuery);
        }

        private string CallApi(string token, string getJson)
        {
            ServiceReference1.WebService1SoapClient client = new ServiceReference1.WebService1SoapClient(ServiceReference1.WebService1SoapClient.EndpointConfiguration.WebService1Soap);

            Task<string> task = client.EDIAPIAsync(token, getJson);

            task.Wait();

            return task.Result;
        }

        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;
            textBox.PasswordChar = '*';

            buttonOk.Text = "確定";
            buttonCancel.Text = "取消";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        private bool IsEmpty(DataRow row)
        {
            if (row == null)
            {
                return true;
            }
            else
            {
                foreach (var value in row.ItemArray)
                {
                    if (value != null && value.ToString() != "")
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string url = ConfigurationManager.AppSettings["PrintA4"];

            if (rbRoller.Checked)
            {
                url = ConfigurationManager.AppSettings["PrintRoller"];
            }
            else if (rb2Size.Checked)
            {
                url = ConfigurationManager.AppSettings["Print2Size"];
            }

//            url = string.Format("{0}?{1}={2}", url, "ids", GetRequestIdByCheckNum());
            string ids = GetRequestIdByCheckNum();

            bool isSetting = true;

            if (printerName.Length == 0)
            {
                isSetting = TriggerSetPrinter();
            }

            ////若無有效設定印表機動作，則不進行列印動作
            if (false == isSetting)
            {
                return;
            }

            ExecutePrintApp(url, ids);
        }

        private void ExecutePrintApp(string url, string ids)
        {
            string szExeFile = ConfigurationManager.AppSettings["exe"];

            string exeFullPath = string.Format("{0}/{1}", Environment.CurrentDirectory, szExeFile);            

            string param = string.Format("{0} {1} {2}", HttpUtility.UrlEncode(url), HttpUtility.UrlEncode(ids), HttpUtility.UrlEncode(printerName));

            System.Diagnostics.Process.Start(exeFullPath, param);
        }

        private bool TriggerSetPrinter()
        {
            bool isSetting = true;

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();
            if (DialogResult.OK == pd.ShowDialog(this))
            {
                SetPrinterSetting(pd.PrinterSettings.PrinterName);
            }
            else
            {
                isSetting = false;
            }

            return isSetting;
        }

        private void SetPrinterSetting(string PrinterName)
        {
            FileInfo fi = new FileInfo("PrinterSetting.txt");

            try
            {
                FileStream fs = new FileStream(fi.FullName, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

                sw.WriteLine(PrinterName);
                printerName = PrinterName;

                sw.Close();
                fs.Close();
            }
            catch (IOException)
            {
                //// 防 init 時檔案使用中
            }

            this.txtPrinter.Text = PrinterName;
        }

        private void GetPrinterSetting()
        {
            //取得印表機設定
            FileInfo fi = new FileInfo("PrinterSetting.txt");

            if (fi.Exists)
            {
                FileStream fs = new FileStream(fi.FullName, FileMode.Open);
                StreamReader sr = new StreamReader(fs, Encoding.UTF8);

                string PrinterStr = sr.ReadLine().Trim();
                this.printerName = PrinterStr;
                this.txtPrinter.Text = PrinterStr;

                sr.Close();
                fs.Close();
            }
        }

        private string GetRequestIdByCheckNum()
        {
            string[] arCheckNums = checkNumbers.Select(c => string.Format("'{0}'", c)).ToArray();
            string checkNumbersString = string.Join(",", arCheckNums);

            string selectQuery = $"SELECT request_id FROM tcDeliveryRequests WHERE check_number in ({checkNumbersString})";

            DataTable dtResult = DbAdaptor.GetDataTableBySql(selectQuery);

            StringBuilder sb = new StringBuilder();
            foreach (DataRow row in dtResult.Rows)
            {
                sb.Append(row["request_id"] + ",");
            }
            string sResult = sb.ToString();

            return sResult.Substring(0, sResult.Length - 1);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            checkNumbers = new List<string>();
            txtProgress.Text += "\r\n清空資料";
            Application.DoEvents();
        }

        private void btnSetPrinter_Click(object sender, EventArgs e)
        {
            TriggerSetPrinter();
        }
    }
}
