﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UploadRequests.Model
{
    public class RequestFieldEntity
    {
        public int LineNumber { get; set; }

        public string checknumber { get; set; }
        public string customercode { get; set; }
        public string ordernumber { get; set; }
        public string receivecustomercode { get; set; }
        public string receivecontact { get; set; }
        public string receivetel1 { get; set; }
        public string receivetel2 { get; set; }
        public string ReceiverAddress { get; set; }
        public string plates { get; set; }
        public string subpoenacategory { get; set; }
        public string collectionmoney { get; set; }
        public string sendcontact { get; set; }
        public string sendtel { get; set; }
        public string SenderAddress { get; set; }
        public string suppliercode { get; set; }
        public string printdate { get; set; }
        public string CbmSize { get; set; }
        public string arriveassigndate { get; set; }
        public string timeperiod { get; set; }
        public string invoicedesc { get; set; }
        public string receiptflag { get; set; }
        public string roundtrip { get; set; }
        public string ArticleNumber { get; set; }
        public string SendPlatform { get; set; }
        public string ArticleName { get; set; }
    }
}
