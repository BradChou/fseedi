﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UploadRequests.Model
{
    public class UserEntity
    {
        public UserEntity()
        {
            IsPreOrderBagUser = false;
        }

        public string CustomerCode { get; set; }

        public string Token { get; set; }

        public long CheckNumStart { get; set; }

        public long CheckNumEnd { get; set; }

        public long CurrentCheckNum { get; set; }

        public bool IsPreOrderBagUser { get; set; }

        public string SendContact { get; set; }

        public string SendTel { get; set; }

        public string SendAddress { get; set; }

        public string SupplierCode { get; set; }
    }
}
