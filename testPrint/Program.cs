﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PdfPrintingNet;

namespace testPrint
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = string.Empty;
            string ids = string.Empty;
            string printerName = string.Empty;

            if (args.Length > 0)
            {
                url = HttpUtility.UrlDecode(args[0]);
                ids = HttpUtility.UrlDecode(args[1]);
                printerName = HttpUtility.UrlDecode(args[2]);
            }

            Console.WriteLine("列印中......");

            var pdfPrint = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");
            pdfPrint.PrinterName = printerName;

            NameValueCollection data = new NameValueCollection();
            data.Add("ids", ids);

            using (WebClient client = new WebClient())
            {
                byte[] pdfByte = client.UploadValues(url, data);
                pdfPrint.Print(pdfByte, "", true);
            }
        }
    }
}
