﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Common
{
    public class DbAdaptor
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

        public static DataTable GetDataTableBySql(string sql)
        {
            DataTable result = new DataTable();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var state = connection.State;

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    try
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message);
                    }
                }
            }

            return result;
        }
        
        public static object ExeScalar(string sql)
        {
            object result = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var state = connection.State;

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    try
                    {
                        result = cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message);
                    }
                }
            }

            return result;
        }

        public static int ExecuteNonQuery(string sql)
        {
            int effectedRow = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var state = connection.State;

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    try
                    {
                        effectedRow = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message);
                    }
                }
            }

            return effectedRow;
        }

        public static void WriteLog(string exceptionMessage)
        {
            // write error to log
        }
    }
}
