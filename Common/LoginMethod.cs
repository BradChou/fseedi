﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace Common
{
    public class LoginMethod
    {
        public static string GetToken(string account, string password)
        {
            // Token
            var response = GetTokenByApi(account, password);
            JObject jObject = JsonConvert.DeserializeObject(response) as JObject;

            if (jObject["error_msg"] != null && jObject["error_msg"].ToString().Length > 0)
                return null;

            return jObject["access_token"].ToString();            
        }

        public static string GetTokenByApi(string account, string password)
        {
            string baseUrl = GetConfig.GetAppSetting("ApiRoot");

            HttpWebRequest request = WebRequest.Create(baseUrl + "/getToken") as HttpWebRequest;
            request.Method = "post";
            request.ContentType = "application/x-www-form-urlencoded";


            NameValueCollection postParams = HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("Accoutno", account);
            postParams.Add("psw", password);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            string responseStr = string.Empty;
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    responseStr = sr.ReadToEnd();
                }
            }

            return responseStr;
        }
    }
}
