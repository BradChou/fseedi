﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace EdiMain
{
    internal class DotNetBarTabControl : TabControl
    {
        private List<Color> color = new List<Color>();
        private List<Color> fontColor = new List<Color>();

        [Category("TabBackGround"), DisplayName("Tab Color")]
        public List<Color> TabBackGround
        {
            get { return color; }
            set
            {
                if (color == value) return;
                color = value;
                Invalidate();
            }
        }

        [Category("TabBackGround"), DisplayName("Tab Font Color")]
        public List<Color> TabFontColor
        {
            get { return fontColor; }
            set
            {
                if (fontColor == value) return;
                fontColor = value;
                Invalidate();
            }
        }

        public DotNetBarTabControl()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer, true);
            SizeMode = TabSizeMode.Fixed;
            ItemSize = new Size(200, 100);
            Alignment = TabAlignment.Left;
            SelectedIndex = 0;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap b = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(b);
            //if (!DesignMode)
            //    SelectedTab.BackColor = Color.White;
            g.Clear(Color.White);

            g.FillRectangle(new SolidBrush(Color.FromArgb(184, 212, 227)), new Rectangle(0, 0, ItemSize.Height + 3, Height));

            for (int i = 0; i <= TabCount - 1; i++)
            {
                if (i == SelectedIndex)
                {
                    Rectangle x2 = new Rectangle(new Point(GetTabRect(i).Location.X - 2, GetTabRect(i).Location.Y - 2),
                        new Size(GetTabRect(i).Width + 3, GetTabRect(i).Height - 1));
                    g.FillRectangle(new SolidBrush(color[i]), x2);

                    Rectangle selectedIndicator = new Rectangle(new Point(GetTabRect(i).Location.X - 2, GetTabRect(i).Location.Y - 2),
                        new Size(10, GetTabRect(i).Height - 1));
                    g.FillRectangle(new SolidBrush(Color.FromArgb(0, 191, 42)), selectedIndicator);

                    g.DrawString(TabPages[i].Text, new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                        new SolidBrush(fontColor[i]), x2, new StringFormat
                        {
                            LineAlignment = StringAlignment.Center,
                            Alignment = StringAlignment.Center
                        });
                }
                else
                {
                    Rectangle x2 = new Rectangle(new Point(GetTabRect(i).Location.X - 2, GetTabRect(i).Location.Y - 2),
                        new Size(GetTabRect(i).Width + 3, GetTabRect(i).Height - 1));
                    g.FillRectangle(new SolidBrush(color[i]), x2);

                    g.DrawString(TabPages[i].Text, Font, new SolidBrush(fontColor[i]), x2, new StringFormat
                    {
                        LineAlignment = StringAlignment.Center,
                        Alignment = StringAlignment.Center
                    });
                }
            }

            e.Graphics.DrawImage(b, new Point(0, 0));
            g.Dispose();
            b.Dispose();
        }
    }
}

