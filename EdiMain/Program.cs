using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EdiMain
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string putParameter = "%7B%22CustomerCode%22%3A%22F2900210002%22%2C%22Token%22%3A%22eyJpbmZvIjp7ImFjY291bnRfY29kZSI6IkYyOTAwMjEwMDAyIn0sImV4cCI6MTYwNzAwMzk3Nn0%3D%22%2C%22CheckNumStart%22%3A0%2C%22CheckNumEnd%22%3A0%2C%22CurrentCheckNum%22%3A0%2C%22IsPreOrderBagUser%22%3Afalse%2C%22SendContact%22%3Anull%2C%22SendTel%22%3Anull%2C%22SendAddress%22%3Anull%2C%22SupplierCode%22%3Anull%7D";

            //string putParameter = string.Empty;

            //if (args.Length > 0)
            //{
            //    putParameter = args[0];
            //}

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main(putParameter));
        }
    }
}
