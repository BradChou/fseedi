using EdiMain.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

public class ComboHeaderCell : DataGridViewColumnHeaderCell
{
    public readonly ComboBox _comboBox;

    public ComboHeaderCell()
    {
        _comboBox = new ComboBox();
        _comboBox.Font = Control.DefaultFont;

        string optionsName = "不指定 訂單編號 收件人姓名＊ 收件人電話＊ 收件人電話(2) 收件人地址＊ 件數＊ 重量(KG)＊ 袋裝(Y/N) 指配日期 指配時段 代收貨款 備註 來回件(Y/N) 回單(Y/N) 產品編號 出貨平台 品名";
        string[] arryOptions = optionsName.Split(' ');

        string optionsVal = "empty,orderNumber,receiveName,receiveTel1,receiveTel2,receiveAddress,peices,weight,bagno,assignDeliveryDate,assignDeliveryTime,collectionMoney,invoiceDesc,roundTrip,returnReceipt,articleNumber,sendPlatform,articleName";
        string[] arryOptionsVal = optionsVal.Split(',');

        IList<RearrangeCombobox> comboboxes = new List<RearrangeCombobox>();

        for (int i = 0; i < arryOptions.Length; i++)
        {
            comboboxes.Add(new RearrangeCombobox { Item = arryOptionsVal[i], DisplayName = arryOptions[i] });
        }

        _comboBox.DataSource = comboboxes;
        _comboBox.DisplayMember = "DisplayName";

        _comboBox.MouseClick += new MouseEventHandler(MouseClick);
        // TODO - Wireup necessary events
    }

    private void MouseClick(object sender, MouseEventArgs e)
    {
        Debug.WriteLine("x:{0}, y:{1}, button:{2}", e.X, e.Y, e.Button);
        OnClick(new DataGridViewCellEventArgs(ColumnIndex, RowIndex));
    }

    public RearrangeCombobox ComboBoxValue
    {
        get { return (RearrangeCombobox)_comboBox.SelectedItem; }
        //set { _comboBox.SelectedText = value; }
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates dataGridViewElementState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
        base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);

        _comboBox.Location = new Point(cellBounds.Left, cellBounds.Top);
        _comboBox.Height = cellBounds.Height;
        _comboBox.Width = cellBounds.Width;

        _comboBox.Height = cellBounds.Height;
        //_comboBox.Width = FixedWidth;
    }
}