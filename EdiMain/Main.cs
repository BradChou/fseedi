﻿using Common;
using EdiMain.Model;
using ExcelDataReader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace EdiMain
{
    public partial class Main : Form
    {
        UserEntity User { get; set; }

        CustomerEntity SelectedCustomer { get; set; }

        DataTable excelData;

        List<RequestFieldEntity> FailedRecord = new List<RequestFieldEntity>();

        List<string> checkNumbers = new List<string>();

        string printerName = string.Empty;

        private bool isCollapsedService = true;
        private bool isCollapsedReport = true;

        private bool selectedAll = false;

        private int indexImportStartPosition = -1;

        private string[] listImportOrder;

        public Main(string parameter)
        {
            InitializeComponent();

            tabControlMain.SelectedTab = tabPageUpload;

            parameter = WebUtility.UrlDecode(parameter);
            User = JsonConvert.DeserializeObject<UserEntity>(parameter);
            SelectedCustomer = new CustomerEntity();

            User.AccountCode = "F2900210002";
            //User.AccountCode = "skyeyes";

            lbGreeting.Text += GetUserName(User.AccountCode);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            InitCustomerComboBox();
            rbSolidFormat_CheckedChanged(null, null);
            cbCustomerTwo_SelectedIndexChanged(cbCustomerTwo, null);
            InitPrintFormatCombo();
            InitPickUpCombo();
            InitEdiReportCombo();
            InitFormatCombo(cbFormatDownload);
        }

        private void dataGridView_HeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dgv = (DataGridView)sender;

            // TODO 第一行

            if (dgv.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn)
            {
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    dgv.Rows[i].Cells[e.ColumnIndex].Value = !selectedAll;
                }

                selectedAll = !selectedAll;
            }
        }

        private void cbCustomerTwo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var customerCode = ((ComboBox)sender).SelectedValue;

            if (customerCode == null)
                return;

            string sql = @$"Select A.request_id, ROW_NUMBER() OVER(order by A.request_id Asc,A.cdate desc , A.check_number desc) AS ROWID,
                B.code_name, A.check_number,A.receive_contact, A.receive_tel1 ,A.receive_city + A.receive_area + A.receive_address as [receive_address],
                A.order_number, case A.holiday_delivery when '1' then '假日配送' else '一般配送' end 'holiday_delivery_chinese',
                A.pieces, A.arrive_assign_date, A.collection_money, A.invoice_desc
                from tcDeliveryRequests A with(nolock)
                left join tbItemCodes B with(nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                left join tbCustomers C with(nolock)  on C.customer_code = A.customer_code
                where A.cancel_date is null and A.Less_than_truckload = 1 and A.DeliveryType = 'D' 
                and A.cdate >= datediff(day, 0, getdate()) and A.cdate < dateadd(day, 1, datediff(day, 0, getdate()))
                and A.customer_code = '{customerCode}'
                order by  A.request_id Asc, A.cdate desc, A.check_number desc";

            DataTable dt = DbAdaptor.GetDataTableBySql(sql);

            BindingSource SBind = new BindingSource();
            SBind.DataSource = dt;

            dgvTodaysTotal.AutoGenerateColumns = false;
            dgvTodaysTotal.DataSource = dt;
            dgvTodaysTotal.DataSource = SBind;
            dgvTodaysTotal.Refresh();

            lbTodaysSummary.Text = $"※今日總筆數：{dt.Rows.Count}";

            int holiday_counter = 0;
            int general_counter = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["holiday_delivery_chinese"].ToString() == "假日配送")
                {
                    holiday_counter += 1;
                }
                else
                {
                    general_counter += 1;
                };
            }

            lbTodaysCountHoliday.Text = $"一般配送: {general_counter} 假日配送: {holiday_counter} *請在週一至週五完成出貨";
        }

        private void picLogout_Click(object sender, EventArgs e)
        {
            string szExeFile = ConfigurationManager.AppSettings["loginExe"];

            string exeFullPath = Path.Combine(Application.StartupPath, szExeFile);

            Process.Start(exeFullPath);

            Application.Exit();
        }

        #region Init
        private void cbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;

            if (cbCustomer.SelectedIndex > -1)
            {
                DataRowView item = (DataRowView)cb.SelectedItem;
                string customerCode = item.Row.ItemArray[0].ToString();

                GetSendCustomerInfo(customerCode);
            }
        }

        private void GetSendCustomerInfo(string customerCode)
        {
            if (customerCode == "")
                return;

            string getCustomerInfo =
                string.Format($@"select top 1 customer_name, telephone, supplier_code, shipments_city + shipments_area + shipments_road as 'senderAddress'
                        from tbCustomers where customer_code = '{customerCode}'");

            DataTable customerInfo = DbAdaptor.GetDataTableBySql(getCustomerInfo);

            SelectedCustomer.AccountCode = customerCode;
            SelectedCustomer.Token = CreateToken(customerCode);
            SelectedCustomer.SendContact = customerInfo.Rows[0].Field<string>("customer_name");
            SelectedCustomer.SendAddress = customerInfo.Rows[0].Field<string>("senderAddress");
            SelectedCustomer.SendTel = customerInfo.Rows[0].Field<string>("telephone");
            string supplierCode = customerInfo.Rows[0].Field<string>("supplier_code");
            SelectedCustomer.SupplierCode = supplierCode;

            lbStationValue.Text = supplierCode;
        }

        private void InitCustomerComboBox()
        {
            if (User.AccountCode == null || User.AccountCode.Length == 0)
                return;

            string managerType = GetManagerType(User.AccountCode);
            User.ManagerType = managerType;

            string sql = "";

            if (managerType == "5")
            {
                sql = $"select customer_code, customer_code + '-' + customer_shortname as show_name from tbCustomers where customer_code = '{User.AccountCode}'";
            }
            else
            {
                string stationCode = GetStationCode();
                User.StationCode = stationCode;

                if (stationCode == null || stationCode.Length == 0)
                    sql = @$"select customer_code, customer_code + '-' + customer_shortname as show_name from tbCustomers where type = 1";
                else
                    sql = @$"select customer_code, customer_code + '-' + customer_shortname as show_name from tbCustomers 
                        where customer_code like '{stationCode}%' and type = 1";
            }

            DataTable data = DbAdaptor.GetDataTableBySql(sql);

            DataRow workRow = data.NewRow();
            workRow.ItemArray = new object[] { "", "---請選擇---" };

            data.Rows.InsertAt(workRow, 0);

            cbCustomer.DataSource = data;
            cbCustomer.DisplayMember = "show_name";
            cbCustomer.ValueMember = "customer_code";

            cbCustomerTwo.DataSource = data.Copy();
            cbCustomerTwo.DisplayMember = "show_name";
            cbCustomerTwo.ValueMember = "customer_code";

            cbCustomerThree.DataSource = data.Copy();
            cbCustomerThree.DisplayMember = "show_name";
            cbCustomerThree.ValueMember = "customer_code";

            cbCustomerFour.DataSource = data.Copy();
            cbCustomerFour.DisplayMember = "show_name";
            cbCustomerFour.ValueMember = "customer_code";
        }

        private string GetManagerType(string accountCode)
        {
            string sqlGetManagerType = $"select manager_type from tbAccounts where account_code = '{accountCode}'";
            var result = DbAdaptor.GetDataTableBySql(sqlGetManagerType);

            return result.Rows[0]["manager_type"].ToString();
        }

        private string GetStationCode()
        {
            string sqlGetManagerType = $"select station_code from tbStation where id = (select station from tbEmps where emp_code = {SelectedCustomer.AccountCode})";
            var result = DbAdaptor.ExeScalar(sqlGetManagerType);

            return result?.ToString();
        }

        private string GetUserName(string accountCode)
        {
            string sqlGetManagerType = $"select user_name from tbAccounts where account_code = '{accountCode}'";
            var result = DbAdaptor.ExeScalar(sqlGetManagerType);

            return result?.ToString();
        }

        private void InitPrintFormatCombo()
        {
            Dictionary<string, string> printFormat = new Dictionary<string, string>();
            printFormat.Add("A4", "A4(一式6筆託運標籤)");
            printFormat.Add("Reel", "捲筒列印");

            cbPaperFormat.DataSource = new BindingSource(printFormat, null);
            cbPaperFormat.DisplayMember = "Value";
            cbPaperFormat.ValueMember = "Key";
        }

        private void InitPickUpCombo()
        {
            Dictionary<string, string> carrier = new Dictionary<string, string>();
            carrier.Add("1", "紙箱");
            carrier.Add("2", "速配袋");

            cbCarrier.DataSource = new BindingSource(carrier, null);
            cbCarrier.DisplayMember = "Value";
            cbCarrier.ValueMember = "Key";

            Dictionary<string, string> type = new Dictionary<string, string>();
            type.Add("1", "箱型貨車");
            type.Add("2", "機車");

            cbPickupType.DataSource = new BindingSource(type, null);
            cbPickupType.DisplayMember = "Value";
            cbPickupType.ValueMember = "Key";

            Dictionary<string, string> receiveTime = new Dictionary<string, string>();
            receiveTime.Add("0", "隨時");
            receiveTime.Add("1", "上午 9:00 ~ 12:00");
            receiveTime.Add("2", "下午 12:00 ~ 18:00");

            cbReceiveTime.DataSource = new BindingSource(receiveTime, null);
            cbReceiveTime.DisplayMember = "Value";
            cbReceiveTime.ValueMember = "Key";
        }

        private void InitEdiReportCombo()
        {
            Dictionary<string, string> scanItem = new Dictionary<string, string>();
            scanItem.Add("*", "全部");
            scanItem.Add("null", "待收件");
            scanItem.Add("5", "集貨");
            scanItem.Add("3", "配達");
            scanItem.Add("2", "配送");
            scanItem.Add("1", "到著");
            scanItem.Add("7", "發送");
            scanItem.Add("6", "卸貨");

            cbScanItem.DataSource = new BindingSource(scanItem, null);
            cbScanItem.DisplayMember = "Value";
            cbScanItem.ValueMember = "Key";
        }

        private void InitFormatCombo(ComboBox comboBox)
        {
            comboBox.Items.Clear();

            string[] fixedFormat = new string[] { "全速配空白託運單", "生活市集空白託運單" };

            string[] existFileName = Directory.GetFiles(Path.Combine(Application.StartupPath, "excel_order"), "*.txt")
                         .Select(Path.GetFileNameWithoutExtension).Where(p => !fixedFormat.Contains(p))
                         .ToArray();

            foreach (var item in existFileName)
            {
                comboBox.Items.Add(item);
            }
        }
        #endregion

        #region 打單
        private void rbSolidFormat_CheckedChanged(object sender, EventArgs e)
        {
            cbFormat.Items.Clear();

            string[] fixedFormat = new string[] { "全速配空白託運單", "生活市集空白託運單" };

            string[] existFileName = Directory.GetFiles(Path.Combine(Application.StartupPath, "excel_order"), "*.txt")
                         .Select(Path.GetFileNameWithoutExtension).Where(p => !fixedFormat.Contains(p))
                         .ToArray();

            if (rbSolidFormat.Checked)
            {
                foreach (var item in fixedFormat)
                {
                    cbFormat.Items.Add(item.Replace(".txt", ""));
                }
            }
            else
            {
                cbFormat.Items.Add("自定義格式");

                foreach (var item in existFileName)
                {
                    cbFormat.Items.Add(item);
                }
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            List<RequestFieldEntity> entities;
            ExcelOrderEntity orderEntity;

            txtFeedBack.Text += "\r\n開始匯入 excel ";
            Application.DoEvents();

            if (cbFormat.SelectedIndex <= -1)
            {
                MessageBox.Show("請選擇匯入格式");
                return;
            }

            if (txtSelectedFile.Text.Length == 0)
            {
                lbFileNotSelect.Visible = true;
                return;
            }

            if (cbFormat.SelectedItem.ToString() == "自定義格式")
            {
                if (listImportOrder == null)
                {
                    tabControlMain.SelectedTab = tabPageRearrange;
                    SetupRearrange();
                    return;
                }

                orderEntity = GetByOrderList(listImportOrder);

                entities = GetRequestEntityFromRearrange(orderEntity);
            }
            else
            {
                string fileName = cbFormat.SelectedItem.ToString() + ".txt";
                string fullFilepath = Path.Combine(Application.StartupPath, "excel_order", fileName);

                string fileContext = File.ReadAllText(fullFilepath);

                string[] text = fileContext.Split("\r\n");

                if (text.Length >= 2)
                {
                    string startPosition = text[1];
                    int.TryParse(startPosition, out indexImportStartPosition);
                }

                string[] order = text[0].Split(',');

                orderEntity = GetByOrderList(order);

                entities = GetRequestEntityFromExcel(orderEntity);
            }

            txtFeedBack.Text += Environment.NewLine + "excel 讀取完成";

            CallApiByEntities(entities);

            listImportOrder = null;

            if (FailedRecord.Count > 0)
            {
                btnTryAgain.Visible = true;
            }
        }

        private List<RequestFieldEntity> GetRequestEntityFromExcel(ExcelOrderEntity orderEntity)
        {
            List<RequestFieldEntity> entities = new List<RequestFieldEntity>();

            for (int i = indexImportStartPosition; i < excelData.Rows.Count; i++)
            {
                DataRow row = excelData.Rows[i];

                if (IsEmpty(row))
                    break;

                RequestFieldEntity request = new RequestFieldEntity() { LineNumber = i };

                request.Parameter = GetByExcel(row, orderEntity);

                string sqlQuery = $"exec LY_AddrFormat @Address = '{request.Parameter.receiverAddress}';";
                DataTable tb = DbAdaptor.GetDataTableBySql(sqlQuery);
                DataRow r = tb.Rows[0];

                request.City = r["city"].ToString();
                request.Area = r["area"].ToString();
                request.Road = r["road"].ToString();

                // 必填欄位
                if (request.Parameter.receiveContact.Trim().Length == 0 || request.Parameter.receiverAddress.Length == 0 || request.Parameter.receivetel1.Length == 0
                    || request.Parameter.plates.Length == 0 || request.Parameter.weight.Length == 0)
                {
                    FailedRecord.Add(request);
                    continue;
                }

                if (request.City.Length == 0 || request.Area.Length == 0)
                {
                    FailedRecord.Add(request);
                    continue;
                }

                entities.Add(request);
            }

            return entities;
        }

        private bool CheckAddress(string address)
        {
            string sqlQuery = $"exec LY_AddrFormat @Address = '{address}';";
            DataTable tb = DbAdaptor.GetDataTableBySql(sqlQuery);
            DataRow r = tb.Rows[0];

            string city = r["city"].ToString();
            string area = r["area"].ToString();

            return (city.Length > 0 && area.Length > 0);
        }

        private List<RequestFieldEntity> GetRequestEntityFromRearrange(ExcelOrderEntity orderEntity)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < dataGridViewExcelData.Columns.Count - 2; i++)
            {
                dt.Columns.Add(new DataColumn());
            }

            List<RequestFieldEntity> checkedFields = new List<RequestFieldEntity>();

            for (int i = indexImportStartPosition; i < dataGridViewExcelData.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridViewExcelData.Rows[i].Cells[1].Value))
                {
                    object[] r = new object[dataGridViewExcelData.Columns.Count - 2];

                    for (int j = 2; j < dataGridViewExcelData.Columns.Count; j++)
                    {
                        r[j - 2] = dataGridViewExcelData.Rows[i].Cells[j].Value;
                    }

                    dt.Rows.Add(r);

                    checkedFields.Add(new RequestFieldEntity { LineNumber = i });
                }
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];

                if (IsEmpty(row))
                    continue;

                ApiParameterEntity parameters = GetByExcel(row, orderEntity);

                // 必填欄位
                if (parameters.receiveContact.Length == 0 || parameters.receiverAddress.Length == 0 || parameters.receivetel1.Length == 0 || parameters.plates.Length == 0 || parameters.weight.Length == 0)
                {
                    FailedRecord.Add(new RequestFieldEntity { LineNumber = i, Parameter = parameters });
                    continue;
                }

                checkedFields[i].Parameter = parameters;
            }

            return checkedFields;
        }
        #endregion

        #region 下載託運單
        private void btnDownloadFile_Click(object sender, EventArgs e)
        {
            string fileUrl = ConfigurationManager.AppSettings["xmlTemplatePath"];
            string fileName = "空白託運單標準格式-零擔.xls";

            DownloadFileFromUrl(fileUrl, fileName);
        }

        private void btnDownload123File_Click(object sender, EventArgs e)
        {
            string fileUrl = ConfigurationManager.AppSettings["xmlTemplate123Path"];
            string fileName = "生活市集空白託運單.xls";

            DownloadFileFromUrl(fileUrl, fileName);
        }

        private void DownloadFileFromUrl(string url, string fileName)
        {
            WebClient client = new WebClient();

            var dialog = new SaveFileDialog();
            dialog.FileName = fileName;
            dialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm|csv|*.csv";
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                client.DownloadFile(url, dialog.FileName);
            }
        }

        private void btnDownloadCustomFormat_Click(object sender, EventArgs e)
        {
            int indexImportStartPosition = 0;

            string fileName = cbFormatDownload.SelectedItem.ToString() + ".txt";
            string fullFilepath = Path.Combine(Application.StartupPath, "excel_order", fileName);

            string fileContext = File.ReadAllText(fullFilepath);

            string[] text = fileContext.Split("\r\n");

            if (text.Length >= 2)
            {
                string startPosition = text[1];
                int.TryParse(startPosition, out indexImportStartPosition);
            }

            string[] order = text[0].Split(',');

            //建立Excel 2007檔案
            IWorkbook wb = new XSSFWorkbook();
            ISheet ws = wb.CreateSheet("Class");

            for (int i = 0; i < indexImportStartPosition - 1; i++)
            {
                ws.CreateRow(i);
                ws.GetRow(i).CreateCell(0).SetCellValue("★注意！空白列勿刪除，標題列勿改動順序");
            }

            ws.CreateRow(indexImportStartPosition - 1);

            for (int i = 0; i < order.Length; i++)
            {
                switch (order[i])
                {
                    case "orderNumber":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("訂單編號");
                        break;
                    case "receiveName":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("收件人姓名(必填)");
                        break;
                    case "receiveTel1":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("收件人電話1(必填)");
                        break;
                    case "receiveTel2":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("收件人電話2");
                        break;
                    case "receiveAddress":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("收件人地址(必填)");
                        break;
                    case "peices":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("件數(必填)");
                        break;
                    case "weight":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("重量(KG)(必填)");
                        break;
                    case "holiday":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("假日配送(Y/N)");
                        break;
                    case "roundTrip":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("來回件(Y/N)");
                        break;
                    case "returnCheckNumber":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("回單(Y/N)");
                        break;
                    case "collectionMoney":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("代收貨款");
                        break;
                    case "invoiceDesc":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("備註(限100字)");
                        break;
                    case "invoiceDesc2":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("備註2");
                        break;
                    case "invoiceDesc3":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("備註3");
                        break;
                    case "assignDeliveryDate":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("指配日期");
                        break;
                    case "assignDeliveryTime":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("指配時段");
                        break;
                    case "articleNumber":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("產品編號");
                        break;
                    case "articleName":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("品名");
                        break;
                    case "sendPlatform":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("出貨平台");
                        break;
                    case "bagNo":
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("袋號");
                        break;
                    default:
                        ws.GetRow(indexImportStartPosition - 1).CreateCell(i).SetCellValue("不指定");
                        break;
                }
            }

            var dialog = new SaveFileDialog();
            dialog.FileName = cbFormatDownload.SelectedItem.ToString();
            dialog.Filter = "Excel |*.xlsx;*.xls";
            var dialogResult = dialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                FileStream fs = new FileStream(dialog.FileName, FileMode.Create);
                wb.Write(fs);
                fs.Close();
            }
        }

        private void btnDeleteCustomFormat_Click(object sender, EventArgs e)
        {
            string fileName = cbFormatDownload.SelectedItem.ToString() + ".txt";
            string fullFilepath = Path.Combine(Application.StartupPath, "excel_order", fileName);

            if (File.Exists(fullFilepath))
            {
                try
                {
                    File.Delete(fullFilepath);
                    MessageBox.Show("刪除成功!");
                    cbFormatDownload.Items.Clear();
                    cbFormatDownload.ResetText();
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }
            }

            InitFormatCombo(cbFormatDownload);
        }
        #endregion

        #region 排一排
        private void SetupRearrange()
        {
            int cols = excelData.Columns.Count - dataGridViewExcelData.ColumnCount + 2;

            for (int i = 0; i < cols; i++)
            {
                dataGridViewExcelData.Columns.Add(new SpecialColumnHeader());
            }

            for (int i = 0; i < excelData.Rows.Count; i++)
            {
                var r = excelData.Rows[i].ItemArray.ToList();
                r.InsertRange(0, new List<object> { null, null });

                dataGridViewExcelData.Rows.Add(r.ToArray());
            }

            dataGridViewExcelData.Width = dataGridViewExcelData.Columns.Count * 150;

            dataGridViewExcelData.Columns[0].DefaultCellStyle.NullValue = null;
        }

        private void dataGridViewExcelData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // 開始位置
            if (e.ColumnIndex == 0)
            {
                DataGridViewCellStyle emptyCellStyle = new DataGridViewCellStyle();

                foreach (DataGridViewRow ele in dataGridViewExcelData.Rows)
                {
                    ele.Cells[0].Style = emptyCellStyle;
                }

                DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
                cellStyle.BackColor = Color.Green;

                int rowIndex = e.RowIndex == 0 ? 1 : e.RowIndex;

                dataGridViewExcelData.Rows[rowIndex].Cells[e.ColumnIndex].Style = cellStyle;
                dataGridViewExcelData.Rows[rowIndex].Cells[e.ColumnIndex].Selected = false;

                indexImportStartPosition = rowIndex;
            }
        }

        private void btnConfirmImport_Click(object sender, EventArgs e)
        {
            selectedAll = false;

            // 確認都有選
            if (!HasSelected())
                return;

            // 取得下拉選單內容
            string strOrder = "";
            List<RearrangeCombobox> listComboOrder = new List<RearrangeCombobox>();

            for (int i = 2; i < dataGridViewExcelData.Columns.Count; i++)
            {
                SpecialColumnHeader header = (SpecialColumnHeader)dataGridViewExcelData.Columns[i];
                var comboValue = header.headerCell.ComboBoxValue;
                strOrder += comboValue.Item + ",";
                listComboOrder.Add(comboValue);
            }
            strOrder += $"\r\n{indexImportStartPosition}";

            listImportOrder = listComboOrder.Select(o => o.Item).ToArray();

            if (!IsComboboxValueOk(listComboOrder))
                return;

            // if 要存格式
            if (checkBoxSaveFormat.Checked)
            {
                string fileName = txtCustomFormatName.Text + ".txt";

                if (!IsFileNameOk(fileName))
                    return;

                string filePath = Path.Combine(Application.StartupPath, "excel_order", fileName);
                File.WriteAllText(filePath, strOrder);

                rbSolidFormat_CheckedChanged(null, null);
                cbFormat.SelectedItem = txtCustomFormatName.Text;

                InitFormatCombo(cbFormatDownload);
            }

            dataGridViewExcelData.Rows.Clear();
            dataGridViewExcelData.Refresh();

            tabControlMain.SelectedTab = tabPageUpload;

            btnUpload_Click(null, null);
        }

        private void btnCancelImport_Click(object sender, EventArgs e)
        {
            dataGridViewExcelData.Rows.Clear();
            dataGridViewExcelData.Refresh();

            tabControlMain.SelectedTab = tabPageUpload;
        }

        private bool IsFileNameOk(string fileName)
        {
            if (fileName.Length == 0)
            {
                MessageBox.Show("請填寫自訂格式名稱");
                return false;
            }

            string[] existFileName = Directory.GetFiles(Path.Combine(Application.StartupPath, "excel_order"), "*.txt")
                                     .Select(Path.GetFileName)
                                     .ToArray();

            if (existFileName.Contains(fileName))
            {
                MessageBox.Show("重複的格式名稱");
                return false;
            }

            return true;
        }

        private bool IsComboboxValueOk(List<RearrangeCombobox> listComboOrder)
        {
            // 必填欄位檢查
            string[] necessaryFields = new string[] { "receiveName", "receiveTel1", "receiveAddress", "peices", "weight" };
            string[] necessaryFieldsName = new string[] { "收件人姓名", "收件人電話", "收件人地址", "件數", "重量" };

            string missingFields = "";
            bool hasMissing = false;

            for (int i = 0; i < necessaryFields.Length; i++)
            {
                if (!listComboOrder.Select(c => c.Item).Contains(necessaryFields[i]))
                {
                    missingFields += $"{necessaryFieldsName[i]} ";
                    hasMissing = true;
                }
            }

            if (hasMissing)
            {
                MessageBox.Show($"{missingFields}未選擇，請進行選擇");
                return false;
            }

            // 重複欄位檢查           
            List<string> repeat = listComboOrder.GroupBy(x => x.Item)
                .Where(g => g.Count() > 1)
                .Select(x => x.Max(c => c.DisplayName)).ToList();

            foreach (var ele in repeat)
            {
                if (ele != "不指定")
                {
                    MessageBox.Show("欄位不可重複");
                    return false;
                }
            }

            return true;
        }

        private bool HasSelected()
        {
            if (indexImportStartPosition < 0)
            {
                MessageBox.Show("請選擇起始位置");
                return false;
            }

            bool flag = false;

            for (int i = indexImportStartPosition; i < dataGridViewExcelData.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridViewExcelData.Rows[i].Cells[1].Value))
                {
                    flag = true;
                }
            }

            if (!flag)
                MessageBox.Show("請勾選要上傳的資料");

            return flag;
        }
        #endregion

        #region Excel 相關
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm|csv|*.csv"
            })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        using (var stream = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                    {
                                        UseHeaderRow = false
                                    }
                                });

                                excelData = result.Tables[0];
                            }
                        }
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("檔案開啟失敗，請確認沒有其他程式正在使用該檔案");
                    }

                    txtSelectedFile.Text = ofd.FileName;
                    lbFileNotSelect.Visible = false;
                }
            }
        }

        private ExcelOrderEntity GetByOrderList(string[] order)
        {
            ExcelOrderEntity orderEntity = new ExcelOrderEntity()
            {
                IndexOrderNumber = Array.IndexOf(order, "orderNumber"),
                IndexReceiveName = Array.IndexOf(order, "receiveName"),
                IndexReceiveTel1 = Array.IndexOf(order, "receiveTel1"),
                IndexReceiveTel2 = Array.IndexOf(order, "receiveTel2"),
                IndexReceiveAddress = Array.IndexOf(order, "receiveAddress"),
                IndexPeices = Array.IndexOf(order, "peices"),
                IndexWeight = Array.IndexOf(order, "weight"),
                IndexHoliday = Array.IndexOf(order, "holiday"),
                IndexRoundTrip = Array.IndexOf(order, "roundTrip"),
                IndexReturnCheckNumber = Array.IndexOf(order, "returnCheckNumber"),
                IndexCollectionMoney = Array.IndexOf(order, "collectionMoney"),
                IndexInvoiceDesc = Array.IndexOf(order, "invoiceDesc"),
                IndexInvoiceDesc2 = Array.IndexOf(order, "invoiceDesc2"),
                IndexInvoiceDesc3 = Array.IndexOf(order, "invoiceDesc3"),
                IndexAssignDeliveryDate = Array.IndexOf(order, "assignDeliveryDate"),
                IndexAssignDeliveryTime = Array.IndexOf(order, "assignDeliveryTime"),
                IndexArticleNumber = Array.IndexOf(order, "articleNumber"),
                IndexArticleName = Array.IndexOf(order, "articleName"),
                IndexSendPlatform = Array.IndexOf(order, "sendPlatform"),
                IndexBagNo = Array.IndexOf(order, "bagNo")
            };

            return orderEntity;
        }

        private ApiParameterEntity GetByExcel(DataRow row, ExcelOrderEntity order)
        {
            string orderNumber = order.IndexOrderNumber == -1 ? "" : row[order.IndexOrderNumber].ToString();
            string receiveName = order.IndexReceiveName == -1 ? "" : row[order.IndexReceiveName].ToString();
            string receiveTel1 = order.IndexReceiveTel1 == -1 ? "" : row[order.IndexReceiveTel1].ToString();
            string receiveTel2 = order.IndexReceiveTel2 == -1 ? "" : row[order.IndexReceiveTel2].ToString();
            string receiveAddress = order.IndexReceiveAddress == -1 ? "" : row[order.IndexReceiveAddress].ToString();
            string peices = order.IndexPeices == -1 ? "1" : row[order.IndexPeices].ToString();
            string weight = order.IndexWeight == -1 ? "" : row[order.IndexWeight].ToString();
            string holiday = order.IndexHoliday == -1 ? "" : row[order.IndexHoliday].ToString();
            string roundTrip = order.IndexRoundTrip == -1 ? "" : row[order.IndexRoundTrip].ToString();
            string returnReceipt = order.IndexReturnCheckNumber == -1 ? "" : row[order.IndexReturnCheckNumber].ToString();
            string collectionMoney = order.IndexCollectionMoney == -1 ? "0" : row[order.IndexCollectionMoney].ToString();
            int collectionMoneyNum;
            string invoiceDesc = order.IndexInvoiceDesc == -1 ? "" : row[order.IndexInvoiceDesc].ToString();
            string invoiceDesc2 = order.IndexInvoiceDesc2 == -1 ? "" : row[order.IndexInvoiceDesc2].ToString();
            string invoiceDesc3 = order.IndexInvoiceDesc3 == -1 ? "" : row[order.IndexInvoiceDesc3].ToString();
            string assignDeliveryDate = order.IndexAssignDeliveryDate == -1 ? "" : row[order.IndexAssignDeliveryDate].ToString();
            DateTime deliveryDate;
            DateTime.TryParse(assignDeliveryDate, out deliveryDate);
            string assignDeliveryTime = order.IndexAssignDeliveryTime == -1 ? "" : row[order.IndexAssignDeliveryTime].ToString();
            string articleNumber = order.IndexArticleNumber == -1 ? "" : row[order.IndexArticleNumber].ToString();
            string sendPlatform = order.IndexSendPlatform == -1 ? "" : row[order.IndexSendPlatform].ToString();
            string articleName = order.IndexArticleName == -1 ? "" : row[order.IndexArticleName].ToString();
            string bagno = order.IndexBagNo == -1 ? "" : row[order.IndexBagNo].ToString();

            string subpoenaCategory = "11";

            if (int.TryParse(collectionMoney, out collectionMoneyNum))
            {
                if (collectionMoneyNum > 0)
                    subpoenaCategory = "41";
            }

            ApiParameterEntity parameterEntity = new ApiParameterEntity
            {
                orderNumber = orderNumber,
                receiptFlag = returnReceipt,
                receiveContact = receiveName,
                receiverAddress = receiveAddress,
                receivetel1 = receiveTel1,
                receivetel2 = receiveTel2,
                plates = peices,
                weight = weight,
                subpoenaCategory = subpoenaCategory,
                collectionMoney = collectionMoney,
                arriveassigndate = deliveryDate == DateTime.MinValue ? "" : deliveryDate.ToString("yyyy/MM/dd HH:mm:ss"),
                timePeriod = assignDeliveryTime,
                sendPlatform = sendPlatform,
                articleName = articleName,
                articleNumber = articleNumber,
                printDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                roundTrip = roundTrip,
                CbmSize = "1",
                InvoiceDesc = invoiceDesc + invoiceDesc2 + invoiceDesc3,
                receiveCustomerCode = "",
                sendContact = SelectedCustomer.SendContact,
                sendTel = SelectedCustomer.SendTel,
                senderAddress = SelectedCustomer.SendAddress,
                customerCode = SelectedCustomer.AccountCode,
                supplierCode = SelectedCustomer.SupplierCode
            };

            return parameterEntity;
        }

        private bool IsEmpty(DataRow row)
        {
            if (row == null)
            {
                return true;
            }
            else
            {
                foreach (var value in row.ItemArray)
                {
                    if (value != null && value.ToString() != "")
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        #endregion

        #region API
        private async Task CallApiByEntities(List<RequestFieldEntity> requestFieldEntities)
        {
            List<RequestFieldEntity> failedRecord = new List<RequestFieldEntity>();

            txtFeedBack.Text += "\r\n開始打單";
            Application.DoEvents();

            Thread backgroundThread = new Thread(
               new ThreadStart(async () =>
               {
                   for (int n = 0; n < requestFieldEntities.Count; n++)
                   {
                       RequestFieldEntity requests = requestFieldEntities[n];
                       if (requests.Parameter == null)
                           continue;

                       string queryString = JsonConvert.SerializeObject(new ApiParameterEntity[] { requests.Parameter });

                       try
                       {
                           string result = await CallApi(SelectedCustomer.Token, queryString);
                           List<EdiApiResponse> jObject = JsonConvert.DeserializeObject<List<EdiApiResponse>>(result);
                           if (jObject[0].result == "true")
                           {
                               checkNumbers.Add(jObject[0].checkNumber);
                           }
                           else
                           {
                               if (jObject[0].msg.Contains("token"))
                               {
                                   SelectedCustomer.Token = CreateToken(SelectedCustomer.AccountCode);
                               }

                               FailedRecord.Add(requests);
                               #region 失敗重打
                               int retryTimes = int.Parse(ConfigurationManager.AppSettings["RetryTimes"]);

                               for (int i = 0; i < retryTimes; i++)
                               {
                                   string r = await CallApi(SelectedCustomer.Token, queryString);

                                   List<EdiApiResponse> response = JsonConvert.DeserializeObject<List<EdiApiResponse>>(result);
                                   if (response[0].result == "true")
                                   {
                                       checkNumbers.Add(response[0].checkNumber);
                                       break;
                                   }
                                   else
                                   {
                                       if (i == retryTimes - 1)
                                           FailedRecord.Add(requests);
                                   }
                               }
                               #endregion

                               // TODO: 顯示打單進度
                               //pbUpload.BeginInvoke(
                               //    new Action(() =>
                               //    {
                               //        pbUpload.Value = j / failedRecord.Count * 100;
                               //    }
                               //));                               
                           }
                       }

                       catch (Exception ex)
                       {
                           txtFeedBack.BeginInvoke(
                                new Action(() =>
                                {
                                    txtFeedBack.Text += "\r\n" + ex.Message;
                                }
                           ));

                           MessageBox.Show(ex.Message);
                           Environment.Exit(Environment.ExitCode);
                       }
                   }

                   string finalMessage = $"\r\n打單程序完成，共有{checkNumbers.Count}筆成功，\r\n共有{failedRecord.Count + FailedRecord.Count}筆失敗：\r\n";

                   foreach (var e in failedRecord)
                   {
                       finalMessage += $"第{e.LineNumber}行, ";
                   }

                   finalMessage += "\r\n---------------------------------------------------------";

                   txtFeedBack.BeginInvoke(
                        new Action(() =>
                        {
                            txtFeedBack.Text += finalMessage;
                        }
                    ));
               }
            ));
            backgroundThread.Start();

            cbCustomerTwo_SelectedIndexChanged(cbCustomerTwo, null);
        }

        private async Task<string> CallApi(string token, string getJson)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.fs-express.com.tw/v1/api/");

            var formdata = new MultipartFormDataContent();
            formdata.Add(new StringContent(token), "Token");
            formdata.Add(new StringContent(getJson), "getJson");
            formdata.Add(new StringContent("false"), "addPickUpRequest");

            var response = await client.PostAsync("EDI", formdata);

            string result = await response.Content.ReadAsStringAsync();

            return result;
        }

        private string CreateToken(string customerCode)
        {
            User user = new User();
            user.account_code = customerCode;
            //產生 Token
            var exp = 3600;   //過期時間(秒)

            //稍微修改 Payload 將使用者資訊和過期時間分開
            var payload = new Payload
            {
                info = user,
                //Unix 時間戳
                exp = Convert.ToInt32((DateTime.Now.AddSeconds(exp) - new DateTime(1970, 1, 1)).TotalSeconds) + 28800 + 600
            };

            var json = JsonConvert.SerializeObject(payload);
            var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));

            return base64;
        }
        #endregion

        #region Menu bar
        private void timerServiceDropDown_Tick(object sender, EventArgs e)
        {
            if (isCollapsedService)
            {
                panelServiceDropDown.Height += 10;
                if (panelServiceDropDown.Size == panelServiceDropDown.MaximumSize)
                {
                    timerServiceDropDown.Stop();
                    isCollapsedService = false;
                }
            }
            else
            {
                panelServiceDropDown.Height -= 10;
                if (panelServiceDropDown.Size == panelServiceDropDown.MinimumSize)
                {
                    timerServiceDropDown.Stop();
                    isCollapsedService = true;
                }
            }
        }

        private void timerReportDropDown_Tick(object sender, EventArgs e)
        {
            if (isCollapsedReport)
            {
                panelReportDropDown.Height += 10;
                if (panelReportDropDown.Size == panelReportDropDown.MaximumSize)
                {
                    timerReportDropDown.Stop();
                    isCollapsedReport = false;
                }
            }
            else
            {
                panelReportDropDown.Height -= 10;
                if (panelReportDropDown.Size == panelReportDropDown.MinimumSize)
                {
                    timerReportDropDown.Stop();
                    isCollapsedReport = true;
                }
            }
        }

        private void btnServiceDropDown_Click(object sender, EventArgs e)
        {
            timerServiceDropDown.Start();
        }

        private void btnReportDropDown_Click(object sender, EventArgs e)
        {
            timerReportDropDown.Start();
        }

        private void btnTabImport_Click(object sender, EventArgs e)
        {
            tabControlMain.SelectTab(0);
            lbSelectedPage.Text = tabControlMain.SelectedTab.Text;
            btnTabImport.BackColor = Color.FromArgb(95, 178, 255);
            btnTabEdiReport.BackColor = Color.FromArgb(166, 213, 255);
        }

        private void btnTabEdiReport_Click(object sender, EventArgs e)
        {
            tabControlMain.SelectTab(2);
            lbSelectedPage.Text = tabControlMain.SelectedTab.Text;
            btnTabEdiReport.BackColor = Color.FromArgb(95, 178, 255);
            btnTabImport.BackColor = Color.FromArgb(166, 213, 255);
        }
        #endregion

        #region 未上傳成功
        private void btnTryAgain_Click(object sender, EventArgs e)
        {
            tabControlMain.SelectedTab = tabPageNotSuccess;

            InitTryAgainDgv(FailedRecord);
        }

        private void InitTryAgainDgv(List<RequestFieldEntity> failedRecord)
        {
            dataGridViewFailedRecord.Rows.Clear();

            for (int i = 0; i < failedRecord.Count; i++)
            {
                ApiParameterEntity p = failedRecord[i].Parameter;
                object[] rowData = new object[] { null, p.orderNumber, p.receiveContact, p.receivetel1, p.receiverAddress, p.plates, p.weight };
                dataGridViewFailedRecord.Rows.Add(rowData);
            }

            for (int j = 0; j < dataGridViewFailedRecord.Rows.Count; j++)
            {
                for (int i = 0; i < dataGridViewFailedRecord.Columns.Count; i++)
                {
                    var val = dataGridViewFailedRecord.Rows[j].Cells[i].Value;

                    if (val == null || val.ToString().Length == 0)
                    {
                        dataGridViewFailedRecord.Rows[j].Cells[i].ReadOnly = false;

                        if (i >= 2)
                            dataGridViewFailedRecord.Rows[j].Cells[i].Style.BackColor = Color.Yellow;
                    }
                    else
                    {
                        dataGridViewFailedRecord.Rows[j].Cells[i].ReadOnly = true;
                    }
                }

                if (failedRecord[j].City == null || failedRecord[j].Area == null || failedRecord[j].City.Length == 0 || failedRecord[j].Area.Length == 0)
                {
                    dataGridViewFailedRecord.Rows[j].Cells[4].ReadOnly = false;
                    dataGridViewFailedRecord.Rows[j].Cells[4].Style.BackColor = Color.Yellow;
                }
            }

            lbNotSuccessCountVal.Text = failedRecord.Count.ToString();
        }

        private void btnUploadAgain_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewFailedRecord.Rows)
            {
                for (int i = 2; i < dataGridViewFailedRecord.Columns.Count; i++)
                {
                    if (row.Cells[i].Value == null || row.Cells[i].Value.ToString().Length == 0)
                    {
                        MessageBox.Show("請填滿所有必填項目");
                        return;
                    }
                }

                string address = row.Cells[4].Value.ToString();
                if (!CheckAddress(address))
                {
                    MessageBox.Show("地址請完整填寫(包括縣市、鄉鎮區)");
                    return;
                }
            }

            // 請勾選
            bool flag = false;

            for (int i = 0; i < dataGridViewFailedRecord.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridViewFailedRecord.Rows[i].Cells[0].Value))
                {
                    flag = true;
                }
            }

            if (!flag)
            {
                MessageBox.Show("請勾選要上傳的資料");
                return;
            }

            List<RequestFieldEntity> failedRecordWithDgvData = FailedRecord;
            List<RequestFieldEntity> retryData = new List<RequestFieldEntity>();

            for (int i = 0; i < dataGridViewFailedRecord.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridViewFailedRecord.Rows[i].Cells[0].Value) == false)
                    continue;

                for (int j = 1; j < dataGridViewFailedRecord.Columns.Count; j++)
                {
                    if (dataGridViewFailedRecord.Rows[i].Cells[j].Value == null)
                        continue;

                    switch (j)
                    {
                        case 2:
                            // 收件姓名
                            failedRecordWithDgvData[i].Parameter.receiveContact = dataGridViewFailedRecord.Rows[i].Cells[j].Value.ToString();
                            break;
                        case 3:
                            // 收件電話
                            failedRecordWithDgvData[i].Parameter.receivetel1 = dataGridViewFailedRecord.Rows[i].Cells[j].Value.ToString();
                            break;
                        case 4:
                            // 收件地址
                            failedRecordWithDgvData[i].Parameter.receiverAddress = dataGridViewFailedRecord.Rows[i].Cells[j].Value.ToString();
                            break;
                        case 5:
                            // 件數
                            failedRecordWithDgvData[i].Parameter.plates = dataGridViewFailedRecord.Rows[i].Cells[j].Value.ToString();
                            break;
                        case 6:
                            // 重量
                            failedRecordWithDgvData[i].Parameter.weight = dataGridViewFailedRecord.Rows[i].Cells[j].Value.ToString();
                            break;
                        default:
                            break;
                    }
                }

                retryData.Add(failedRecordWithDgvData[i]);
            }

            tabControlMain.SelectedTab = tabPageUpload;

            FailedRecord = new List<RequestFieldEntity>();
            btnTryAgain.Visible = false;

            CallApiByEntities(retryData);
        }

        private void btnRemoveFailedData_Click(object sender, EventArgs e)
        {
            FailedRecord = new List<RequestFieldEntity>();
            tabControlMain.SelectedTab = tabPageUpload;
        }
        #endregion

        #region 今日總筆數勾選區間
        private void rbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            if (rb.Checked)
            {
                selectedAll = false;

                for (int i = 0; i < dgvTodaysTotal.RowCount; i++)
                {
                    dgvTodaysTotal.Rows[i].Cells[0].Value = !selectedAll;
                }
            }
            else
            {
                selectedAll = true;

                for (int i = 0; i < dgvTodaysTotal.RowCount; i++)
                {
                    dgvTodaysTotal.Rows[i].Cells[0].Value = !selectedAll;
                }

                numericPrintStart.Value = 0;
                numericPrintEnd.Value = 0;
            }
        }

        private void dgvTodaysTotal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;

            if (dgv.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn)
            {
                bool all = true;

                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (Convert.ToBoolean(dgv.Rows[i].Cells[e.ColumnIndex].Value) == false)
                        all = false;
                }

                if (all)
                {
                    rbSelectAll.Checked = true;
                }
                else
                {
                    rbNotSelectAll.Checked = true;
                }
            }
        }

        private void numericPrintStart_ValueChanged(object sender, EventArgs e)
        {
            SelectCheckBox();
        }

        private void numericPrintEnd_ValueChanged(object sender, EventArgs e)
        {
            SelectCheckBox();
        }

        private void SelectCheckBox()
        {
            rbNotSelectAll.Checked = true;

            if (numericPrintStart.Value <= numericPrintEnd.Value)
            {
                for (int i = 0; i < dgvTodaysTotal.Rows.Count; i++)
                {
                    dgvTodaysTotal.Rows[i].Cells[0].Value = false;
                }

                for (int i = (int)numericPrintStart.Value - 1; i < (int)numericPrintEnd.Value; i++)
                {
                    if (i < 0)
                        i = 0;
                    dgvTodaysTotal.Rows[i].Cells[0].Value = true;
                }
            }
        }
        #endregion

        #region 列印
        private void Print(string print_requestid, string printFormat, string startPos = "0")
        {
            NameValueCollection nvcParamters = new NameValueCollection();
            nvcParamters["ids"] = print_requestid;

            if (!cbPrintShowSender.Checked)
                nvcParamters["showSender"] = "false";

            string url = "";

            if (printFormat == "Reel")
            {
                url = "https://erp.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0";
            }
            else
            {
                url = "http://erp.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_0";
                nvcParamters["position"] = startPos;
            }

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] result = client.UploadValues(url, "POST", nvcParamters);

                var dialog = new SaveFileDialog();
                dialog.FileName = "Label";
                dialog.Filter = "Pdf Files|*.pdf";
                var dialogResult = dialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    File.WriteAllBytes(dialog.FileName, result);
                }
            }
        }

        private void btnPrintLabel_Click(object sender, EventArgs e)
        {
            // get checked request ids
            List<string> requestIds = new List<string>();

            for (int i = 0; i < dgvTodaysTotal.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvTodaysTotal.Rows[i].Cells[0].Value))
                {
                    requestIds.Add(dgvTodaysTotal.Rows[i].Cells["Column21"].Value.ToString());
                }
            }

            // get format, position
            string format = cbPaperFormat.SelectedValue.ToString();
            string position = "0";

            var buttons = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);

            if (rbPos1.Checked)
                position = "1";
            if (rbPos2.Checked)
                position = "2";
            if (rbPos3.Checked)
                position = "3";
            if (rbPos4.Checked)
                position = "4";
            if (rbPos5.Checked)
                position = "5";
            if (rbPos6.Checked)
                position = "6";

            // print
            Print(string.Join(",", requestIds), format, position);
        }

        private void btnDownloadRequestExcel_Click(object sender, EventArgs e)
        {
            List<string> checkNumbers = new List<string>();

            for (int i = 0; i < dgvTodaysTotal.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvTodaysTotal.Rows[i].Cells[0].Value))
                {
                    string checkNum = dgvTodaysTotal.Rows[i].Cells["Column11"].Value.ToString();
                    checkNumbers.Add($"'{checkNum}'");
                }
            }

            if (checkNumbers.Count == 0)
            {
                MessageBox.Show("沒有勾選");
                return;
            }

            string strCheckNum = string.Join(',', checkNumbers);

            CreateExcel(strCheckNum);
        }

        private void CreateExcel(string check_number)
        {
            string sheet_title = "FSE託運總表";
            string file_name = "FSE託運總表" + DateTime.Now.ToString("yyyyMMdd");
            int totalcount = 0;
            int totalpieces = 0;

            string wherestr = " and check_number in(" + check_number + ")";
            string orderby = " order by A.check_number";

            string sql = $@"Select
        ROW_NUMBER() OVER(ORDER BY A.check_number) '序號',
        A.order_number '訂單編號' , 
        A.check_number '貨號' , 
        A.receive_contact '收件人',  
        ISNULL(A.pieces,0) '件數',
        A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
        E.station_name  '到著站',
        A.receive_tel1 '收件人電話',  
        A.collection_money '代收金額',
        B.code_name '付款別',
        A.SendPlatform '平臺名稱',
        A.ArticleName '商品名稱',
        A.invoice_desc '備註'  
        from tcDeliveryRequests A with(nolock)
        left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
        left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
        Left join tbSuppliers D with(nolock) on A.area_arrive_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
        Left join tbStation E with(nolock) on A.area_arrive_code = E.station_scode
        where A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {wherestr} {orderby}";

            DataTable dt = DbAdaptor.GetDataTableBySql(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    totalpieces = totalpieces + Convert.ToInt32(dt.Rows[i]["件數"].ToString());
                }
                int rowIndex = 0;
                ISheet sheet = null;
                totalcount = dt.Rows.Count;

                IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）

                IRow headerRow = null;

                if (string.IsNullOrEmpty(sheet_title))
                {
                    sheet = workbook.CreateSheet();
                }
                else
                {
                    sheet = workbook.CreateSheet(sheet_title);
                }

                ICellStyle style = workbook.CreateCellStyle();

                var font1 = workbook.CreateFont();
                font1.Boldweight = 30;
                font1.FontHeightInPoints = 24;
                style.SetFont(font1);

                IRow titletop = sheet.CreateRow(rowIndex++);
                ICell cell = sheet.CreateRow(0).CreateCell(0);
                cell.SetCellValue("FSE託運總表");
                cell.CellStyle = style;

                IRow dataRowDate = sheet.CreateRow(rowIndex++);
                dataRowDate.CreateCell(0).SetCellValue("發送日期");
                dataRowDate.CreateCell(1).SetCellValue(DateTime.Now.ToString("yyyy-MM-dd"));

                rowIndex++;

                headerRow = sheet.CreateRow(rowIndex++);

                #region 處理標題列
                foreach (DataColumn column in dt.Columns)
                {
                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                }
                #endregion

                #region 處理資料列
                foreach (DataRow row in dt.Rows)
                {
                    IRow dataRow = sheet.CreateRow(rowIndex++);

                    foreach (DataColumn column in dt.Columns)
                    {
                        dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));
                        ICellStyle styleA = workbook.CreateCellStyle();
                    }
                }
                IRow dataRowA = sheet.CreateRow(rowIndex + 1);

                dataRowA.CreateCell(1).SetCellValue(Convert.ToString("總計"));
                dataRowA.CreateCell(2).SetCellValue(Convert.ToString("共" + totalcount + "筆"));
                dataRowA.CreateCell(3).SetCellValue(Convert.ToString("共" + totalpieces + "件"));

                sheet.SetColumnWidth(0, 15 * 256);//序號寬度
                sheet.SetColumnWidth(1, 30 * 256);//訂單編號寬度
                sheet.SetColumnWidth(2, 30 * 256);//貨號寬度
                sheet.SetColumnWidth(3, 30 * 256);//收件人寬度
                sheet.SetColumnWidth(4, 10 * 256);//件數寬度
                sheet.SetColumnWidth(5, 100 * 256);//地址寬度
                sheet.SetColumnWidth(6, 25 * 256);//到著站寬度
                sheet.SetColumnWidth(7, 30 * 256);//收件人電話寬度
                sheet.SetColumnWidth(8, 10 * 256);//代收金額寬度
                sheet.SetColumnWidth(9, 10 * 256);//付款別寬度
                sheet.SetColumnWidth(10, 30 * 256);//平臺名稱寬度
                sheet.SetColumnWidth(11, 30 * 256);//商品名稱寬度
                sheet.SetColumnWidth(12, 60 * 256);//備註寬度
                #endregion

                var dialog = new SaveFileDialog();
                dialog.FileName = file_name;
                dialog.Filter = "Excel |*.xlsx;*.xls";
                var dialogResult = dialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    FileStream file = new FileStream(dialog.FileName, FileMode.Create);
                    workbook.Write(file);
                    file.Close();
                }
            }
        }
        #endregion

        #region 派員收件
        private void SavePickUpRequest(string customerCode, string pieces, string note, string carrier, string pickUpType, string receiveTime)
        {
            string strNow = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            string sql = $@"insert into pick_up_request_log (request_customer_code, pick_up_pieces, remark, 
                package_type, vechile_type, cdate, udate, pick_up_date)
                values ('{customerCode}', {pieces}, '{note}', '{carrier}', '{pickUpType}', '{strNow}', '{strNow}', '{receiveTime}')";

            int effectiveRow = DbAdaptor.ExecuteNonQuery(sql);

            string theDay = "今天";
            DateTime day = DateTime.Now.Date;
            if (DateTime.Now.Hour >= 16)
            {
                theDay = "明天";
                day = day.AddDays(1);
            }

            string alertPickupTime = day.ToString("MM/dd");

            if (effectiveRow > 0)
            {
                MessageBox.Show($"派件成功，司機將在{theDay}({alertPickupTime}) 12:00~18:00 到場收件");
            }
        }

        private void btnSendPickupRequest_Click(object sender, EventArgs e)
        {
            string customerCode = cbCustomerThree.SelectedValue.ToString();
            string pieces = numericPickupCount.Text;
            string note = txtPickUpMemo.Text;
            string carrier = cbCarrier.SelectedValue.ToString();
            string pickupType = cbPickupType.SelectedValue.ToString();
            string receiveTime = cbReceiveTime.SelectedValue.ToString();

            SavePickUpRequest(customerCode, pieces, note, carrier, pickupType, receiveTime);

            tabControlMain.SelectedTab = tabPageUpload;
        }

        private void btnGotoPickupRequest_Click(object sender, EventArgs e)
        {
            if (cbCustomerThree.SelectedIndex <= 0)
            {
                MessageBox.Show("請選擇客代");
                return;
            }

            tabControlMain.SelectedTab = tabPagePickupRequest;
        }
        #endregion

        #region EDI 訂單管理
        private void btnReportSearch_Click(object sender, EventArgs e)
        {
            // 檢查選項

            string printDateStart = dtPrintDateStart.Value.ToString("yyyy/MM/dd");
            string printDateEnd = dtPrintDateEnd.Value.ToString("yyyy/MM/dd");

            string customerCode = cbCustomerFour.SelectedValue.ToString();

            string scanItem = cbScanItem.SelectedValue.ToString();
            string scanItemQuery = $" AND A.latest_scan_item = '{scanItem}'";
            if (scanItem == "null")
                scanItemQuery = $" AND _arr.arrive_state is null";
            else if (scanItem == "*")
                scanItemQuery = "";

            string deliveryType = "D";
            if (rbReverseDelivery.Checked)
                deliveryType = "R";

            string checkNumber = txtCheckNum.Text;

            string sql = $@"
SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO,
A.print_date, CONVERT(CHAR(10),A.supplier_date,111) as 'supplier_date', A.check_number, A.order_number,sta1.station_name as 'send_station' ,A.send_contact,A.receive_contact,
CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END 'receiveTel'
,A.receive_city, A.receive_area,A.cbmWeight,A.pieces,
CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end 'receiveAddress'
--,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end 'sendAddress'
,A.area_arrive_code,G.station_name as 'receive_station',B.code_name,ISNULL(A.collection_money,0) as 'collection_money',A.invoice_desc,
_arr.arrive_state as arrive_state, _arr.newest_scandate as newest_scandate, A.request_id
FROM tcDeliveryRequests A With(Nolock) 
LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code = A.supplier_code	
CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
WHERE 
A.print_date IS NOT NULL AND A.Less_than_truckload= 1 
AND (A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R') )
AND A.print_date >= '{printDateStart}' and A.print_date <= '{printDateEnd}' 
AND ('{checkNumber}' = '' or A.Check_number = '{checkNumber}') 
AND A.customer_code = '{customerCode}' 
AND A.DeliveryType = '{deliveryType}' 
{scanItemQuery}
";

            DataTable dt = DbAdaptor.GetDataTableBySql(sql);
            dgvDeliveryRequest.DataSource = dt;
            BindingSource SBind = new BindingSource();
            SBind.DataSource = dt;

            dgvDeliveryRequest.AutoGenerateColumns = false;
            dgvDeliveryRequest.DataSource = SBind;
            dgvDeliveryRequest.Refresh();

            // 件數
            int totalCount = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                totalCount += dt.Rows[i].Field<int>("pieces");
            }
            lbReportTotalCountValue.Text = totalCount.ToString();
        }

        private void btnReportExport_Click(object sender, EventArgs e)
        {
            if (dgvDeliveryRequest.DataSource == null)
            {
                MessageBox.Show("請先查詢");
                return;
            }

            DataTable data = (DataTable)((BindingSource)dgvDeliveryRequest.DataSource).DataSource;

            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Excel(2007-2013)|*.xlsx";
            fileDialog.FileName = $"EDI_report{DateTime.Today.ToString("yyyyMMdd")}";
            string sheetName = "report";
            if (fileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet(sheetName);
            IRow rowHead = sheet.CreateRow(0);

            //填寫表頭
            for (int i = 0; i < dgvDeliveryRequest.Columns.Count - 1; i++)
            {
                rowHead.CreateCell(i, CellType.String).SetCellValue(dgvDeliveryRequest.Columns[i].HeaderText);
            }

            //填寫內容
            for (int i = 0; i < data.Rows.Count; i++)
            {
                IRow row = sheet.CreateRow(i + 1);
                for (int j = 0; j < data.Columns.Count - 1; j++)
                {
                    row.CreateCell(j, CellType.String).SetCellValue(data.Rows[i][j].ToString());
                }
            }

            for (int i = 0; i < data.Columns.Count; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            using (FileStream stream = File.OpenWrite(fileDialog.FileName))
            {
                workbook.Write(stream);
                stream.Close();
            }

            GC.Collect();
        }
        #endregion
    }
}
