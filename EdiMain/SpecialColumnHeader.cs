using System.Windows.Forms;

public class SpecialColumnHeader : DataGridViewTextBoxColumn
{
    public ComboHeaderCell headerCell { get; set; }

    public SpecialColumnHeader()
    {
        //this.CellTemplate = new DataGridViewTextBoxCell();
        headerCell = new ComboHeaderCell();
        this.HeaderCell = headerCell;
        this.Width = 150;
    }

    protected override void OnDataGridViewChanged()
    {
        if (DataGridView != null)
        {
            this.DataGridView.Controls.Add(headerCell._comboBox);
        }
    }        
}