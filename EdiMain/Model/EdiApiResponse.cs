﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class EdiApiResponse
    {
        public string result { get; set; }

        public string msg { get; set; }

        public string checkNumber { get; set; }

        public string sd { get; set; }

        public string md { get; set; }

        public string putorder { get; set; }

        public string area { get; set; }
    }
}
