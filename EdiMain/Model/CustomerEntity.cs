﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class CustomerEntity
    {
        public CustomerEntity()
        {
            IsPreOrderBagUser = false;
        }

        public string AccountCode { get; set; }

        public string Token { get; set; }

        public bool IsPreOrderBagUser { get; set; }

        public string SendContact { get; set; }

        public string SendTel { get; set; }

        public string SendAddress { get; set; }

        public string SupplierCode { get; set; }
    }

    public class Payload
    {
        //使用者資訊
        public User info { get; set; }
        //過期時間
        public int exp { get; set; }
    }

    public class User
    {
        public string account_code { get; set; }
    }
}
