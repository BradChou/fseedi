﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class UserEntity
    {
        public string AccountCode { get; set; }

        public string Token { get; set; }

        public string StationCode { get; set; }

        public string ManagerType { get; set; }
    }
}
