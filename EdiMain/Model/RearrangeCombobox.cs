﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class RearrangeCombobox
    {
        public string Item { get; set; }

        public string DisplayName { get; set; }
    }
}
