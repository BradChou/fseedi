﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class RequestFieldEntity
    {
        public int LineNumber { get; set; }       

        public ApiParameterEntity Parameter { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public string Road { get; set; }
    }
}
