﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class ApiParameterEntity
    {
        public ApiParameterEntity()
        {
            checkNumber = "";
            collectionMoney = "0";
        }
        public string checkNumber { get; set; }
        public string customerCode { get; set; }
        public string orderNumber { get; set; }
        public string receiveCustomerCode { get; set; }
        public string receiveContact { get; set; }
        public string receivetel1 { get; set; }
        public string receivetel2 { get; set; }
        public string receiverAddress { get; set; }
        public string plates { get; set; }
        public string weight { get; set; }
        public string subpoenaCategory { get; set; }
        public string collectionMoney { get; set; }
        public string sendContact { get; set; }
        public string sendTel { get; set; }
        public string senderAddress { get; set; }
        public string supplierCode { get; set; }
        public string printDate { get; set; }
        public string CbmSize { get; set; }
        public string arriveassigndate { get; set; }
        public string timePeriod { get; set; }
        public string InvoiceDesc { get; set; }
        public string receiptFlag { get; set; }
        public string roundTrip { get; set; }
        public string articleNumber { get; set; }
        public string sendPlatform { get; set; }
        public string articleName { get; set; }
    }
}
