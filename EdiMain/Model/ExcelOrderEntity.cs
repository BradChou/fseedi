﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EdiMain.Model
{
    public class ExcelOrderEntity
    {
        public int StartPosition { get; set; }

        public int IndexOrderNumber { get; set; }
        public int IndexReceiveName { get; set; }
        public int IndexReceiveTel1 { get; set; }
        public int IndexReceiveTel2 { get; set; }
        public int IndexReceiveAddress { get; set; }
        public int IndexPeices { get; set; }
        public int IndexWeight { get; set; }
        public int IndexHoliday { get; set; }
        public int IndexRoundTrip { get; set; }
        public int IndexReturnCheckNumber { get; set; }
        public int IndexCollectionMoney { get; set; }
        public int IndexInvoiceDesc { get; set; }
        public int IndexInvoiceDesc2 { get; set; }
        public int IndexInvoiceDesc3 { get; set; }
        public int IndexAssignDeliveryDate { get; set; }
        public int IndexAssignDeliveryTime { get; set; }
        public int IndexArticleNumber { get; set; }
        public int IndexArticleName { get; set; }
        public int IndexSendPlatform { get; set; }
        public int IndexBagNo { get; set; }
    }
}
