﻿using EdiMain.Model;
using System.Collections.Generic;

namespace EdiMain
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panelUpload = new System.Windows.Forms.Panel();
            this.btnTryAgain = new EdiMain.Model.RoundedButton();
            this.lbFileNotSelect = new System.Windows.Forms.Label();
            this.txtFeedBack = new System.Windows.Forms.TextBox();
            this.btnUpload = new EdiMain.Model.RoundedButton();
            this.btnSelectFile = new EdiMain.Model.RoundedButton();
            this.txtSelectedFile = new System.Windows.Forms.TextBox();
            this.lbFormat = new System.Windows.Forms.Label();
            this.lbFormatCategory = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.lbCusotmer = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.rbCustomFormat = new System.Windows.Forms.RadioButton();
            this.rbSolidFormat = new System.Windows.Forms.RadioButton();
            this.dateOrigin = new System.Windows.Forms.DateTimePicker();
            this.cbCustomer = new System.Windows.Forms.ComboBox();
            this.lbUploadTitle = new System.Windows.Forms.Label();
            this.lbDownloadTitle = new System.Windows.Forms.Label();
            this.panelDownload = new System.Windows.Forms.Panel();
            this.btnDeleteCustomFormat = new System.Windows.Forms.Button();
            this.btnDownloadCustomFormat = new System.Windows.Forms.Button();
            this.cbFormatDownload = new System.Windows.Forms.ComboBox();
            this.lbDownloadWarning = new System.Windows.Forms.Label();
            this.btnDownloadFile = new EdiMain.Model.RoundedButton();
            this.btnDownload123File = new EdiMain.Model.RoundedButton();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.lbMessageContent = new System.Windows.Forms.Label();
            this.panelTitleText = new System.Windows.Forms.Panel();
            this.picLogout = new System.Windows.Forms.PictureBox();
            this.lbGreeting = new System.Windows.Forms.Label();
            this.lbSelectedPage = new System.Windows.Forms.Label();
            this.panelInfromation = new System.Windows.Forms.Panel();
            this.lbMessage = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageUpload = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbCustomerThree = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbPos1 = new System.Windows.Forms.RadioButton();
            this.rbPos2 = new System.Windows.Forms.RadioButton();
            this.rbPos3 = new System.Windows.Forms.RadioButton();
            this.rbPos5 = new System.Windows.Forms.RadioButton();
            this.rbPos6 = new System.Windows.Forms.RadioButton();
            this.rbPos4 = new System.Windows.Forms.RadioButton();
            this.cbPrintShowSender = new System.Windows.Forms.CheckBox();
            this.btnDownloadRequestExcel = new System.Windows.Forms.Button();
            this.btnGotoPickupRequest = new System.Windows.Forms.Button();
            this.btnPrintLabel = new System.Windows.Forms.Button();
            this.lbPrintStartPos = new System.Windows.Forms.Label();
            this.cbPaperFormat = new System.Windows.Forms.ComboBox();
            this.lbPaperFormat = new System.Windows.Forms.Label();
            this.lbPrint = new System.Windows.Forms.Label();
            this.dgvTodaysTotal = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numericPrintEnd = new System.Windows.Forms.NumericUpDown();
            this.numericPrintStart = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.rbNotSelectAll = new System.Windows.Forms.RadioButton();
            this.rbSelectAll = new System.Windows.Forms.RadioButton();
            this.cbCustomerTwo = new System.Windows.Forms.ComboBox();
            this.lbTodaysCountHoliday = new System.Windows.Forms.Label();
            this.lbTodaysSummary = new System.Windows.Forms.Label();
            this.tabPageRearrange = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCancelImport = new System.Windows.Forms.Button();
            this.btnConfirmImport = new System.Windows.Forms.Button();
            this.txtCustomFormatName = new System.Windows.Forms.TextBox();
            this.checkBoxSaveFormat = new System.Windows.Forms.CheckBox();
            this.dataGridViewExcelData = new System.Windows.Forms.DataGridView();
            this.start = new System.Windows.Forms.DataGridViewImageColumn();
            this.selectAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lbRearrange = new System.Windows.Forms.Label();
            this.tabPageEdiReport = new System.Windows.Forms.TabPage();
            this.dgvDeliveryRequest = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.originDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendStation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendCustomer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.peices = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaArriveCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arriveStation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subpoenaCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collectionMoney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScanItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScanDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequestId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReportExport = new System.Windows.Forms.Button();
            this.btnReportSearch = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCheckNum = new System.Windows.Forms.TextBox();
            this.cbScanItem = new System.Windows.Forms.ComboBox();
            this.cbCustomerFour = new System.Windows.Forms.ComboBox();
            this.dtPrintDateEnd = new System.Windows.Forms.DateTimePicker();
            this.dtPrintDateStart = new System.Windows.Forms.DateTimePicker();
            this.lbReportFilterShipdate = new System.Windows.Forms.Label();
            this.lbReportFilterCustomerCode = new System.Windows.Forms.Label();
            this.lbReportFilterScanItem = new System.Windows.Forms.Label();
            this.lbReportFilterCheckNumber = new System.Windows.Forms.Label();
            this.lbReportTotalCountValue = new System.Windows.Forms.Label();
            this.lbStationValue = new System.Windows.Forms.Label();
            this.lbTotalCount = new System.Windows.Forms.Label();
            this.lbStation = new System.Windows.Forms.Label();
            this.rbReverseDelivery = new System.Windows.Forms.RadioButton();
            this.rbDelivery = new System.Windows.Forms.RadioButton();
            this.lbType = new System.Windows.Forms.Label();
            this.tabPageNotSuccess = new System.Windows.Forms.TabPage();
            this.lbNotSuccessCountVal = new System.Windows.Forms.Label();
            this.btnRemoveFailedData = new System.Windows.Forms.Button();
            this.btnUploadAgain = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewFailedRecord = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbNotSuccessCount = new System.Windows.Forms.Label();
            this.tabPagePickupRequest = new System.Windows.Forms.TabPage();
            this.numericPickupCount = new System.Windows.Forms.NumericUpDown();
            this.cbReceiveTime = new System.Windows.Forms.ComboBox();
            this.cbPickupType = new System.Windows.Forms.ComboBox();
            this.cbCarrier = new System.Windows.Forms.ComboBox();
            this.btnSendPickupRequest = new System.Windows.Forms.Button();
            this.txtPickUpMemo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbTodayShipCount = new System.Windows.Forms.Label();
            this.LeftPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panelServiceDropDown = new System.Windows.Forms.Panel();
            this.btnTabImport = new System.Windows.Forms.Button();
            this.btnServiceDropDown = new System.Windows.Forms.Button();
            this.panelReportDropDown = new System.Windows.Forms.Panel();
            this.btnTabEdiReport = new System.Windows.Forms.Button();
            this.btnReportDropDown = new System.Windows.Forms.Button();
            this.timerServiceDropDown = new System.Windows.Forms.Timer(this.components);
            this.timerReportDropDown = new System.Windows.Forms.Timer(this.components);
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.panelUpload.SuspendLayout();
            this.panelDownload.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.panelTitleText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogout)).BeginInit();
            this.panelInfromation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.tabControlMain.SuspendLayout();
            this.tabPageUpload.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTodaysTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrintEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrintStart)).BeginInit();
            this.tabPageRearrange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcelData)).BeginInit();
            this.tabPageEdiReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryRequest)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPageNotSuccess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFailedRecord)).BeginInit();
            this.tabPagePickupRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPickupCount)).BeginInit();
            this.LeftPanel.SuspendLayout();
            this.panelServiceDropDown.SuspendLayout();
            this.panelReportDropDown.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelUpload
            // 
            this.panelUpload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUpload.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelUpload.Controls.Add(this.btnTryAgain);
            this.panelUpload.Controls.Add(this.lbFileNotSelect);
            this.panelUpload.Controls.Add(this.txtFeedBack);
            this.panelUpload.Controls.Add(this.btnUpload);
            this.panelUpload.Controls.Add(this.btnSelectFile);
            this.panelUpload.Controls.Add(this.txtSelectedFile);
            this.panelUpload.Controls.Add(this.lbFormat);
            this.panelUpload.Controls.Add(this.lbFormatCategory);
            this.panelUpload.Controls.Add(this.lbShipDate);
            this.panelUpload.Controls.Add(this.lbCusotmer);
            this.panelUpload.Controls.Add(this.cbFormat);
            this.panelUpload.Controls.Add(this.rbCustomFormat);
            this.panelUpload.Controls.Add(this.rbSolidFormat);
            this.panelUpload.Controls.Add(this.dateOrigin);
            this.panelUpload.Controls.Add(this.cbCustomer);
            this.panelUpload.Location = new System.Drawing.Point(82, 344);
            this.panelUpload.Name = "panelUpload";
            this.panelUpload.Size = new System.Drawing.Size(6129, 535);
            this.panelUpload.TabIndex = 4;
            // 
            // btnTryAgain
            // 
            this.btnTryAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTryAgain.BorderColor = System.Drawing.Color.Transparent;
            this.btnTryAgain.BorderDownColor = System.Drawing.Color.Empty;
            this.btnTryAgain.BorderDownWidth = 0F;
            this.btnTryAgain.BorderOverColor = System.Drawing.Color.Empty;
            this.btnTryAgain.BorderOverWidth = 0F;
            this.btnTryAgain.BorderRadius = 10;
            this.btnTryAgain.BorderWidth = 0F;
            this.btnTryAgain.ForeColor = System.Drawing.Color.White;
            this.btnTryAgain.Location = new System.Drawing.Point(54, 502);
            this.btnTryAgain.Name = "btnTryAgain";
            this.btnTryAgain.Size = new System.Drawing.Size(483, 23);
            this.btnTryAgain.TabIndex = 11;
            this.btnTryAgain.Text = "修正未上傳的資料";
            this.btnTryAgain.UseVisualStyleBackColor = false;
            this.btnTryAgain.Visible = false;
            this.btnTryAgain.Click += new System.EventHandler(this.btnTryAgain_Click);
            // 
            // lbFileNotSelect
            // 
            this.lbFileNotSelect.AutoSize = true;
            this.lbFileNotSelect.ForeColor = System.Drawing.Color.DarkRed;
            this.lbFileNotSelect.Location = new System.Drawing.Point(485, 206);
            this.lbFileNotSelect.Name = "lbFileNotSelect";
            this.lbFileNotSelect.Size = new System.Drawing.Size(79, 15);
            this.lbFileNotSelect.TabIndex = 13;
            this.lbFileNotSelect.Text = "尚未選擇檔案";
            this.lbFileNotSelect.Visible = false;
            // 
            // txtFeedBack
            // 
            this.txtFeedBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFeedBack.Location = new System.Drawing.Point(54, 308);
            this.txtFeedBack.Multiline = true;
            this.txtFeedBack.Name = "txtFeedBack";
            this.txtFeedBack.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFeedBack.Size = new System.Drawing.Size(829, 166);
            this.txtFeedBack.TabIndex = 12;
            // 
            // btnUpload
            // 
            this.btnUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnUpload.BorderColor = System.Drawing.Color.Transparent;
            this.btnUpload.BorderDownColor = System.Drawing.Color.Empty;
            this.btnUpload.BorderDownWidth = 0F;
            this.btnUpload.BorderOverColor = System.Drawing.Color.Empty;
            this.btnUpload.BorderOverWidth = 0F;
            this.btnUpload.BorderRadius = 10;
            this.btnUpload.BorderWidth = 0F;
            this.btnUpload.ForeColor = System.Drawing.Color.White;
            this.btnUpload.Location = new System.Drawing.Point(54, 251);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(483, 23);
            this.btnUpload.TabIndex = 11;
            this.btnUpload.Text = "上傳";
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnSelectFile.BorderColor = System.Drawing.Color.Black;
            this.btnSelectFile.BorderDownColor = System.Drawing.Color.Empty;
            this.btnSelectFile.BorderDownWidth = 0F;
            this.btnSelectFile.BorderOverColor = System.Drawing.Color.Empty;
            this.btnSelectFile.BorderOverWidth = 0F;
            this.btnSelectFile.BorderRadius = 10;
            this.btnSelectFile.BorderWidth = 0F;
            this.btnSelectFile.ForeColor = System.Drawing.Color.White;
            this.btnSelectFile.Location = new System.Drawing.Point(377, 202);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(91, 23);
            this.btnSelectFile.TabIndex = 10;
            this.btnSelectFile.Text = "選擇";
            this.btnSelectFile.UseVisualStyleBackColor = false;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtSelectedFile
            // 
            this.txtSelectedFile.Location = new System.Drawing.Point(54, 203);
            this.txtSelectedFile.Name = "txtSelectedFile";
            this.txtSelectedFile.PlaceholderText = "請匯入要選擇的檔案，支援xls.xlsx.csv";
            this.txtSelectedFile.Size = new System.Drawing.Size(291, 23);
            this.txtSelectedFile.TabIndex = 9;
            // 
            // lbFormat
            // 
            this.lbFormat.AutoSize = true;
            this.lbFormat.Location = new System.Drawing.Point(54, 153);
            this.lbFormat.Name = "lbFormat";
            this.lbFormat.Size = new System.Drawing.Size(116, 15);
            this.lbFormat.TabIndex = 8;
            this.lbFormat.Text = "★.請選擇託運單格式";
            // 
            // lbFormatCategory
            // 
            this.lbFormatCategory.AutoSize = true;
            this.lbFormatCategory.Location = new System.Drawing.Point(54, 111);
            this.lbFormatCategory.Name = "lbFormatCategory";
            this.lbFormatCategory.Size = new System.Drawing.Size(44, 15);
            this.lbFormatCategory.TabIndex = 7;
            this.lbFormatCategory.Text = "★.格式";
            // 
            // lbShipDate
            // 
            this.lbShipDate.AutoSize = true;
            this.lbShipDate.Location = new System.Drawing.Point(54, 69);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(68, 15);
            this.lbShipDate.TabIndex = 6;
            this.lbShipDate.Text = "★.發送日期";
            // 
            // lbCusotmer
            // 
            this.lbCusotmer.AutoSize = true;
            this.lbCusotmer.Location = new System.Drawing.Point(54, 21);
            this.lbCusotmer.Name = "lbCusotmer";
            this.lbCusotmer.Size = new System.Drawing.Size(128, 15);
            this.lbCusotmer.TabIndex = 5;
            this.lbCusotmer.Text = "★.請選擇要匯入的客代";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(216, 150);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(321, 23);
            this.cbFormat.TabIndex = 4;
            // 
            // rbCustomFormat
            // 
            this.rbCustomFormat.AutoSize = true;
            this.rbCustomFormat.Location = new System.Drawing.Point(356, 109);
            this.rbCustomFormat.Name = "rbCustomFormat";
            this.rbCustomFormat.Size = new System.Drawing.Size(85, 19);
            this.rbCustomFormat.TabIndex = 3;
            this.rbCustomFormat.Text = "自定義格式";
            this.rbCustomFormat.UseVisualStyleBackColor = true;
            // 
            // rbSolidFormat
            // 
            this.rbSolidFormat.AutoSize = true;
            this.rbSolidFormat.Checked = true;
            this.rbSolidFormat.Location = new System.Drawing.Point(216, 111);
            this.rbSolidFormat.Name = "rbSolidFormat";
            this.rbSolidFormat.Size = new System.Drawing.Size(73, 19);
            this.rbSolidFormat.TabIndex = 2;
            this.rbSolidFormat.TabStop = true;
            this.rbSolidFormat.Text = "固定格式";
            this.rbSolidFormat.UseVisualStyleBackColor = true;
            this.rbSolidFormat.CheckedChanged += new System.EventHandler(this.rbSolidFormat_CheckedChanged);
            // 
            // dateOrigin
            // 
            this.dateOrigin.Enabled = false;
            this.dateOrigin.Location = new System.Drawing.Point(216, 63);
            this.dateOrigin.Name = "dateOrigin";
            this.dateOrigin.Size = new System.Drawing.Size(321, 23);
            this.dateOrigin.TabIndex = 1;
            // 
            // cbCustomer
            // 
            this.cbCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbCustomer.FormattingEnabled = true;
            this.cbCustomer.Location = new System.Drawing.Point(216, 18);
            this.cbCustomer.Name = "cbCustomer";
            this.cbCustomer.Size = new System.Drawing.Size(321, 23);
            this.cbCustomer.TabIndex = 0;
            this.cbCustomer.SelectedIndexChanged += new System.EventHandler(this.cbCustomer_SelectedIndexChanged);
            // 
            // lbUploadTitle
            // 
            this.lbUploadTitle.AutoSize = true;
            this.lbUploadTitle.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbUploadTitle.Location = new System.Drawing.Point(35, 291);
            this.lbUploadTitle.Name = "lbUploadTitle";
            this.lbUploadTitle.Size = new System.Drawing.Size(135, 30);
            this.lbUploadTitle.TabIndex = 3;
            this.lbUploadTitle.Text = "2. 上傳檔案";
            // 
            // lbDownloadTitle
            // 
            this.lbDownloadTitle.AutoSize = true;
            this.lbDownloadTitle.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbDownloadTitle.Location = new System.Drawing.Point(42, 25);
            this.lbDownloadTitle.Name = "lbDownloadTitle";
            this.lbDownloadTitle.Size = new System.Drawing.Size(207, 30);
            this.lbDownloadTitle.TabIndex = 3;
            this.lbDownloadTitle.Text = "1. 下載空白託運單";
            // 
            // panelDownload
            // 
            this.panelDownload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDownload.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelDownload.Controls.Add(this.btnDeleteCustomFormat);
            this.panelDownload.Controls.Add(this.btnDownloadCustomFormat);
            this.panelDownload.Controls.Add(this.cbFormatDownload);
            this.panelDownload.Controls.Add(this.lbDownloadWarning);
            this.panelDownload.Controls.Add(this.btnDownloadFile);
            this.panelDownload.Controls.Add(this.btnDownload123File);
            this.panelDownload.Location = new System.Drawing.Point(75, 74);
            this.panelDownload.Name = "panelDownload";
            this.panelDownload.Size = new System.Drawing.Size(6129, 196);
            this.panelDownload.TabIndex = 2;
            // 
            // btnDeleteCustomFormat
            // 
            this.btnDeleteCustomFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteCustomFormat.FlatAppearance.BorderSize = 0;
            this.btnDeleteCustomFormat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCustomFormat.ForeColor = System.Drawing.Color.White;
            this.btnDeleteCustomFormat.Location = new System.Drawing.Point(474, 70);
            this.btnDeleteCustomFormat.Name = "btnDeleteCustomFormat";
            this.btnDeleteCustomFormat.Size = new System.Drawing.Size(108, 32);
            this.btnDeleteCustomFormat.TabIndex = 5;
            this.btnDeleteCustomFormat.Text = "刪除自訂範本";
            this.btnDeleteCustomFormat.UseVisualStyleBackColor = false;
            this.btnDeleteCustomFormat.Click += new System.EventHandler(this.btnDeleteCustomFormat_Click);
            // 
            // btnDownloadCustomFormat
            // 
            this.btnDownloadCustomFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnDownloadCustomFormat.FlatAppearance.BorderSize = 0;
            this.btnDownloadCustomFormat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownloadCustomFormat.ForeColor = System.Drawing.Color.White;
            this.btnDownloadCustomFormat.Location = new System.Drawing.Point(356, 70);
            this.btnDownloadCustomFormat.Name = "btnDownloadCustomFormat";
            this.btnDownloadCustomFormat.Size = new System.Drawing.Size(85, 32);
            this.btnDownloadCustomFormat.TabIndex = 5;
            this.btnDownloadCustomFormat.Text = "下載csv檔";
            this.btnDownloadCustomFormat.UseVisualStyleBackColor = false;
            this.btnDownloadCustomFormat.Click += new System.EventHandler(this.btnDownloadCustomFormat_Click);
            // 
            // cbFormatDownload
            // 
            this.cbFormatDownload.FormattingEnabled = true;
            this.cbFormatDownload.Location = new System.Drawing.Point(25, 76);
            this.cbFormatDownload.Name = "cbFormatDownload";
            this.cbFormatDownload.Size = new System.Drawing.Size(298, 23);
            this.cbFormatDownload.TabIndex = 4;
            // 
            // lbDownloadWarning
            // 
            this.lbDownloadWarning.AutoSize = true;
            this.lbDownloadWarning.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbDownloadWarning.Location = new System.Drawing.Point(25, 128);
            this.lbDownloadWarning.Name = "lbDownloadWarning";
            this.lbDownloadWarning.Size = new System.Drawing.Size(588, 38);
            this.lbDownloadWarning.TabIndex = 2;
            this.lbDownloadWarning.Text = "※第一次使用請下載空白託運表單標準格式\r\n※版本更新日期：2021/01/25，若您匯入版本相較於本日期為舊，請先下載空白託運單";
            // 
            // btnDownloadFile
            // 
            this.btnDownloadFile.BackColor = System.Drawing.Color.White;
            this.btnDownloadFile.BorderColor = System.Drawing.Color.Black;
            this.btnDownloadFile.BorderDownColor = System.Drawing.Color.Empty;
            this.btnDownloadFile.BorderDownWidth = 0F;
            this.btnDownloadFile.BorderOverColor = System.Drawing.Color.Empty;
            this.btnDownloadFile.BorderOverWidth = 0F;
            this.btnDownloadFile.BorderRadius = 10;
            this.btnDownloadFile.BorderWidth = 1F;
            this.btnDownloadFile.ForeColor = System.Drawing.Color.Black;
            this.btnDownloadFile.Location = new System.Drawing.Point(25, 20);
            this.btnDownloadFile.Name = "btnDownloadFile";
            this.btnDownloadFile.Size = new System.Drawing.Size(122, 23);
            this.btnDownloadFile.TabIndex = 0;
            this.btnDownloadFile.Text = "全速配空白託運單";
            this.btnDownloadFile.UseVisualStyleBackColor = false;
            this.btnDownloadFile.Click += new System.EventHandler(this.btnDownloadFile_Click);
            // 
            // btnDownload123File
            // 
            this.btnDownload123File.BackColor = System.Drawing.Color.White;
            this.btnDownload123File.BorderColor = System.Drawing.Color.Black;
            this.btnDownload123File.BorderDownColor = System.Drawing.Color.Empty;
            this.btnDownload123File.BorderDownWidth = 1F;
            this.btnDownload123File.BorderOverColor = System.Drawing.Color.Empty;
            this.btnDownload123File.BorderOverWidth = 0F;
            this.btnDownload123File.BorderRadius = 10;
            this.btnDownload123File.BorderWidth = 1F;
            this.btnDownload123File.CausesValidation = false;
            this.btnDownload123File.Location = new System.Drawing.Point(192, 20);
            this.btnDownload123File.Name = "btnDownload123File";
            this.btnDownload123File.Size = new System.Drawing.Size(131, 23);
            this.btnDownload123File.TabIndex = 1;
            this.btnDownload123File.Text = "生活市集空白託運單";
            this.btnDownload123File.UseVisualStyleBackColor = false;
            this.btnDownload123File.Click += new System.EventHandler(this.btnDownload123File_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHeader.BackColor = System.Drawing.Color.White;
            this.panelHeader.Controls.Add(this.lbMessageContent);
            this.panelHeader.Controls.Add(this.panelTitleText);
            this.panelHeader.Controls.Add(this.panelInfromation);
            this.panelHeader.Location = new System.Drawing.Point(228, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1104, 85);
            this.panelHeader.TabIndex = 1;
            // 
            // lbMessageContent
            // 
            this.lbMessageContent.AutoSize = true;
            this.lbMessageContent.Location = new System.Drawing.Point(125, 6);
            this.lbMessageContent.Name = "lbMessageContent";
            this.lbMessageContent.Size = new System.Drawing.Size(132, 15);
            this.lbMessageContent.TabIndex = 3;
            this.lbMessageContent.Text = "歡迎使用EDI系統 (v1.7)";
            // 
            // panelTitleText
            // 
            this.panelTitleText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTitleText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(187)))));
            this.panelTitleText.Controls.Add(this.picLogout);
            this.panelTitleText.Controls.Add(this.lbGreeting);
            this.panelTitleText.Controls.Add(this.lbSelectedPage);
            this.panelTitleText.Location = new System.Drawing.Point(0, 25);
            this.panelTitleText.Name = "panelTitleText";
            this.panelTitleText.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.panelTitleText.Size = new System.Drawing.Size(1104, 60);
            this.panelTitleText.TabIndex = 1;
            // 
            // picLogout
            // 
            this.picLogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.picLogout.Image = ((System.Drawing.Image)(resources.GetObject("picLogout.Image")));
            this.picLogout.Location = new System.Drawing.Point(1024, 0);
            this.picLogout.Margin = new System.Windows.Forms.Padding(3, 3, 300, 3);
            this.picLogout.Name = "picLogout";
            this.picLogout.Size = new System.Drawing.Size(60, 60);
            this.picLogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogout.TabIndex = 2;
            this.picLogout.TabStop = false;
            this.picLogout.Click += new System.EventHandler(this.picLogout_Click);
            // 
            // lbGreeting
            // 
            this.lbGreeting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGreeting.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbGreeting.ForeColor = System.Drawing.Color.White;
            this.lbGreeting.Location = new System.Drawing.Point(769, 25);
            this.lbGreeting.Name = "lbGreeting";
            this.lbGreeting.Size = new System.Drawing.Size(227, 19);
            this.lbGreeting.TabIndex = 1;
            this.lbGreeting.Text = "歡迎 ";
            this.lbGreeting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSelectedPage
            // 
            this.lbSelectedPage.AutoSize = true;
            this.lbSelectedPage.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbSelectedPage.ForeColor = System.Drawing.Color.White;
            this.lbSelectedPage.Location = new System.Drawing.Point(36, 20);
            this.lbSelectedPage.Name = "lbSelectedPage";
            this.lbSelectedPage.Size = new System.Drawing.Size(143, 24);
            this.lbSelectedPage.TabIndex = 0;
            this.lbSelectedPage.Text = "匯入託運單資料";
            // 
            // panelInfromation
            // 
            this.panelInfromation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.panelInfromation.Controls.Add(this.lbMessage);
            this.panelInfromation.Location = new System.Drawing.Point(0, -1);
            this.panelInfromation.Name = "panelInfromation";
            this.panelInfromation.Size = new System.Drawing.Size(119, 28);
            this.panelInfromation.TabIndex = 2;
            this.panelInfromation.Tag = "即時訊息";
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.ForeColor = System.Drawing.Color.White;
            this.lbMessage.Location = new System.Drawing.Point(33, 7);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(55, 15);
            this.lbMessage.TabIndex = 0;
            this.lbMessage.Text = "即時訊息";
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.White;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(3, 3);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(224, 115);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageUpload);
            this.tabControlMain.Controls.Add(this.tabPageRearrange);
            this.tabControlMain.Controls.Add(this.tabPageEdiReport);
            this.tabControlMain.Controls.Add(this.tabPageNotSuccess);
            this.tabControlMain.Controls.Add(this.tabPagePickupRequest);
            this.tabControlMain.Location = new System.Drawing.Point(233, 50);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 4;
            this.tabControlMain.Size = new System.Drawing.Size(1099, 759);
            this.tabControlMain.TabIndex = 0;
            // 
            // tabPageUpload
            // 
            this.tabPageUpload.AutoScroll = true;
            this.tabPageUpload.BackColor = System.Drawing.Color.White;
            this.tabPageUpload.Controls.Add(this.panelUpload);
            this.tabPageUpload.Controls.Add(this.lbDownloadTitle);
            this.tabPageUpload.Controls.Add(this.lbUploadTitle);
            this.tabPageUpload.Controls.Add(this.panelDownload);
            this.tabPageUpload.Controls.Add(this.panel2);
            this.tabPageUpload.Location = new System.Drawing.Point(4, 24);
            this.tabPageUpload.Name = "tabPageUpload";
            this.tabPageUpload.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUpload.Size = new System.Drawing.Size(1091, 731);
            this.tabPageUpload.TabIndex = 0;
            this.tabPageUpload.Text = "匯入託運單資料";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbCustomerThree);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.cbPrintShowSender);
            this.panel2.Controls.Add(this.btnDownloadRequestExcel);
            this.panel2.Controls.Add(this.btnGotoPickupRequest);
            this.panel2.Controls.Add(this.btnPrintLabel);
            this.panel2.Controls.Add(this.lbPrintStartPos);
            this.panel2.Controls.Add(this.cbPaperFormat);
            this.panel2.Controls.Add(this.lbPaperFormat);
            this.panel2.Controls.Add(this.lbPrint);
            this.panel2.Controls.Add(this.dgvTodaysTotal);
            this.panel2.Controls.Add(this.numericPrintEnd);
            this.panel2.Controls.Add(this.numericPrintStart);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.rbNotSelectAll);
            this.panel2.Controls.Add(this.rbSelectAll);
            this.panel2.Controls.Add(this.cbCustomerTwo);
            this.panel2.Controls.Add(this.lbTodaysCountHoliday);
            this.panel2.Controls.Add(this.lbTodaysSummary);
            this.panel2.Location = new System.Drawing.Point(42, 937);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1211, 834);
            this.panel2.TabIndex = 5;
            // 
            // cbCustomerThree
            // 
            this.cbCustomerThree.FormattingEnabled = true;
            this.cbCustomerThree.Location = new System.Drawing.Point(40, 774);
            this.cbCustomerThree.Name = "cbCustomerThree";
            this.cbCustomerThree.Size = new System.Drawing.Size(250, 23);
            this.cbCustomerThree.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.ForeColor = System.Drawing.Color.DarkRed;
            this.label9.Location = new System.Drawing.Point(33, 723);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(537, 20);
            this.label9.TabIndex = 3;
            this.label9.Text = "下午四點前提出派員收件要求，當日收貨；下午四點以後提出將在次日收貨";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(2, 681);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 30);
            this.label7.TabIndex = 3;
            this.label7.Text = "5. 出貨";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbPos1);
            this.groupBox1.Controls.Add(this.rbPos2);
            this.groupBox1.Controls.Add(this.rbPos3);
            this.groupBox1.Controls.Add(this.rbPos5);
            this.groupBox1.Controls.Add(this.rbPos6);
            this.groupBox1.Controls.Add(this.rbPos4);
            this.groupBox1.Location = new System.Drawing.Point(558, 477);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(82, 126);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // rbPos1
            // 
            this.rbPos1.AutoSize = true;
            this.rbPos1.Checked = true;
            this.rbPos1.Location = new System.Drawing.Point(16, 23);
            this.rbPos1.Name = "rbPos1";
            this.rbPos1.Size = new System.Drawing.Size(14, 13);
            this.rbPos1.TabIndex = 13;
            this.rbPos1.TabStop = true;
            this.rbPos1.UseVisualStyleBackColor = true;
            // 
            // rbPos2
            // 
            this.rbPos2.AutoSize = true;
            this.rbPos2.Location = new System.Drawing.Point(50, 23);
            this.rbPos2.Name = "rbPos2";
            this.rbPos2.Size = new System.Drawing.Size(14, 13);
            this.rbPos2.TabIndex = 13;
            this.rbPos2.TabStop = true;
            this.rbPos2.UseVisualStyleBackColor = true;
            // 
            // rbPos3
            // 
            this.rbPos3.AutoSize = true;
            this.rbPos3.Location = new System.Drawing.Point(16, 56);
            this.rbPos3.Name = "rbPos3";
            this.rbPos3.Size = new System.Drawing.Size(14, 13);
            this.rbPos3.TabIndex = 13;
            this.rbPos3.TabStop = true;
            this.rbPos3.UseVisualStyleBackColor = true;
            // 
            // rbPos5
            // 
            this.rbPos5.AutoSize = true;
            this.rbPos5.Location = new System.Drawing.Point(16, 88);
            this.rbPos5.Name = "rbPos5";
            this.rbPos5.Size = new System.Drawing.Size(14, 13);
            this.rbPos5.TabIndex = 13;
            this.rbPos5.TabStop = true;
            this.rbPos5.UseVisualStyleBackColor = true;
            // 
            // rbPos6
            // 
            this.rbPos6.AutoSize = true;
            this.rbPos6.Location = new System.Drawing.Point(50, 88);
            this.rbPos6.Name = "rbPos6";
            this.rbPos6.Size = new System.Drawing.Size(14, 13);
            this.rbPos6.TabIndex = 13;
            this.rbPos6.TabStop = true;
            this.rbPos6.UseVisualStyleBackColor = true;
            // 
            // rbPos4
            // 
            this.rbPos4.AutoSize = true;
            this.rbPos4.Location = new System.Drawing.Point(50, 56);
            this.rbPos4.Name = "rbPos4";
            this.rbPos4.Size = new System.Drawing.Size(14, 13);
            this.rbPos4.TabIndex = 13;
            this.rbPos4.TabStop = true;
            this.rbPos4.UseVisualStyleBackColor = true;
            // 
            // cbPrintShowSender
            // 
            this.cbPrintShowSender.AutoSize = true;
            this.cbPrintShowSender.Checked = true;
            this.cbPrintShowSender.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPrintShowSender.Location = new System.Drawing.Point(59, 538);
            this.cbPrintShowSender.Name = "cbPrintShowSender";
            this.cbPrintShowSender.Size = new System.Drawing.Size(110, 19);
            this.cbPrintShowSender.TabIndex = 14;
            this.cbPrintShowSender.Text = "顯示寄件人資料";
            this.cbPrintShowSender.UseVisualStyleBackColor = true;
            // 
            // btnDownloadRequestExcel
            // 
            this.btnDownloadRequestExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnDownloadRequestExcel.FlatAppearance.BorderSize = 0;
            this.btnDownloadRequestExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownloadRequestExcel.ForeColor = System.Drawing.Color.White;
            this.btnDownloadRequestExcel.Location = new System.Drawing.Point(205, 586);
            this.btnDownloadRequestExcel.Name = "btnDownloadRequestExcel";
            this.btnDownloadRequestExcel.Size = new System.Drawing.Size(99, 34);
            this.btnDownloadRequestExcel.TabIndex = 12;
            this.btnDownloadRequestExcel.Text = "列印託運總表";
            this.btnDownloadRequestExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownloadRequestExcel.UseVisualStyleBackColor = false;
            this.btnDownloadRequestExcel.Click += new System.EventHandler(this.btnDownloadRequestExcel_Click);
            // 
            // btnGotoPickupRequest
            // 
            this.btnGotoPickupRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnGotoPickupRequest.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnGotoPickupRequest.FlatAppearance.BorderSize = 0;
            this.btnGotoPickupRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGotoPickupRequest.ForeColor = System.Drawing.Color.White;
            this.btnGotoPickupRequest.Location = new System.Drawing.Point(331, 767);
            this.btnGotoPickupRequest.Name = "btnGotoPickupRequest";
            this.btnGotoPickupRequest.Size = new System.Drawing.Size(99, 34);
            this.btnGotoPickupRequest.TabIndex = 12;
            this.btnGotoPickupRequest.Text = "派員收件";
            this.btnGotoPickupRequest.UseVisualStyleBackColor = false;
            this.btnGotoPickupRequest.Click += new System.EventHandler(this.btnGotoPickupRequest_Click);
            // 
            // btnPrintLabel
            // 
            this.btnPrintLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnPrintLabel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPrintLabel.FlatAppearance.BorderSize = 0;
            this.btnPrintLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintLabel.ForeColor = System.Drawing.Color.White;
            this.btnPrintLabel.Location = new System.Drawing.Point(55, 586);
            this.btnPrintLabel.Name = "btnPrintLabel";
            this.btnPrintLabel.Size = new System.Drawing.Size(99, 34);
            this.btnPrintLabel.TabIndex = 12;
            this.btnPrintLabel.Text = "列印標籤";
            this.btnPrintLabel.UseVisualStyleBackColor = false;
            this.btnPrintLabel.Click += new System.EventHandler(this.btnPrintLabel_Click);
            // 
            // lbPrintStartPos
            // 
            this.lbPrintStartPos.AutoSize = true;
            this.lbPrintStartPos.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPrintStartPos.Location = new System.Drawing.Point(413, 496);
            this.lbPrintStartPos.Name = "lbPrintStartPos";
            this.lbPrintStartPos.Size = new System.Drawing.Size(129, 19);
            this.lbPrintStartPos.TabIndex = 10;
            this.lbPrintStartPos.Text = "選擇列印起始位置";
            // 
            // cbPaperFormat
            // 
            this.cbPaperFormat.FormattingEnabled = true;
            this.cbPaperFormat.Location = new System.Drawing.Point(135, 496);
            this.cbPaperFormat.Name = "cbPaperFormat";
            this.cbPaperFormat.Size = new System.Drawing.Size(155, 23);
            this.cbPaperFormat.TabIndex = 11;
            this.cbPaperFormat.Tag = "";
            // 
            // lbPaperFormat
            // 
            this.lbPaperFormat.AutoSize = true;
            this.lbPaperFormat.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPaperFormat.Location = new System.Drawing.Point(55, 500);
            this.lbPaperFormat.Name = "lbPaperFormat";
            this.lbPaperFormat.Size = new System.Drawing.Size(69, 19);
            this.lbPaperFormat.TabIndex = 10;
            this.lbPaperFormat.Text = "紙張格式";
            // 
            // lbPrint
            // 
            this.lbPrint.AutoSize = true;
            this.lbPrint.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbPrint.Location = new System.Drawing.Point(2, 461);
            this.lbPrint.Name = "lbPrint";
            this.lbPrint.Size = new System.Drawing.Size(87, 30);
            this.lbPrint.TabIndex = 3;
            this.lbPrint.Text = "4. 列印";
            // 
            // dgvTodaysTotal
            // 
            this.dgvTodaysTotal.AllowUserToAddRows = false;
            this.dgvTodaysTotal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTodaysTotal.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvTodaysTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTodaysTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21});
            this.dgvTodaysTotal.Location = new System.Drawing.Point(9, 202);
            this.dgvTodaysTotal.Name = "dgvTodaysTotal";
            this.dgvTodaysTotal.Size = new System.Drawing.Size(1136, 225);
            this.dgvTodaysTotal.TabIndex = 9;
            this.dgvTodaysTotal.Text = "dataGridView1";
            this.dgvTodaysTotal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTodaysTotal_CellContentClick);
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "colCheckBox";
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.Width = 30;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "ROWID";
            this.Column9.HeaderText = "No.";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 50;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "code_name";
            this.Column10.HeaderText = "傳票類別";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 60;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "check_number";
            this.Column11.HeaderText = "貨號";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "receive_contact";
            this.Column12.HeaderText = "收件人";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "receive_tel1";
            this.Column13.HeaderText = "收件人電話";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "receive_address";
            this.Column14.HeaderText = "收件人地址";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "order_number";
            this.Column15.HeaderText = "訂單編號";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "holiday_delivery_chinese";
            this.Column16.HeaderText = "指定配送";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "pieces";
            this.Column17.HeaderText = "件數";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 40;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "arrive_assign_date";
            this.Column18.HeaderText = "指配日期";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "collection_money";
            this.Column19.HeaderText = "代收貨款";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "invoice_desc";
            this.Column20.HeaderText = "備註";
            this.Column20.Name = "Column20";
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "request_id";
            this.Column21.HeaderText = "";
            this.Column21.Name = "Column21";
            this.Column21.Visible = false;
            // 
            // numericPrintEnd
            // 
            this.numericPrintEnd.Location = new System.Drawing.Point(225, 159);
            this.numericPrintEnd.Name = "numericPrintEnd";
            this.numericPrintEnd.Size = new System.Drawing.Size(65, 23);
            this.numericPrintEnd.TabIndex = 8;
            this.numericPrintEnd.ValueChanged += new System.EventHandler(this.numericPrintEnd_ValueChanged);
            // 
            // numericPrintStart
            // 
            this.numericPrintStart.Location = new System.Drawing.Point(97, 159);
            this.numericPrintStart.Name = "numericPrintStart";
            this.numericPrintStart.Size = new System.Drawing.Size(65, 23);
            this.numericPrintStart.TabIndex = 8;
            this.numericPrintStart.ValueChanged += new System.EventHandler(this.numericPrintStart_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(180, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "至";
            // 
            // rbNotSelectAll
            // 
            this.rbNotSelectAll.AutoSize = true;
            this.rbNotSelectAll.Location = new System.Drawing.Point(40, 159);
            this.rbNotSelectAll.Name = "rbNotSelectAll";
            this.rbNotSelectAll.Size = new System.Drawing.Size(49, 19);
            this.rbNotSelectAll.TabIndex = 6;
            this.rbNotSelectAll.TabStop = true;
            this.rbNotSelectAll.Text = "從：";
            this.rbNotSelectAll.UseVisualStyleBackColor = true;
            // 
            // rbSelectAll
            // 
            this.rbSelectAll.AutoSize = true;
            this.rbSelectAll.Location = new System.Drawing.Point(40, 124);
            this.rbSelectAll.Name = "rbSelectAll";
            this.rbSelectAll.Size = new System.Drawing.Size(49, 19);
            this.rbSelectAll.TabIndex = 6;
            this.rbSelectAll.TabStop = true;
            this.rbSelectAll.Text = "全選";
            this.rbSelectAll.UseVisualStyleBackColor = true;
            this.rbSelectAll.CheckedChanged += new System.EventHandler(this.rbSelectAll_CheckedChanged);
            // 
            // cbCustomerTwo
            // 
            this.cbCustomerTwo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbCustomerTwo.FormattingEnabled = true;
            this.cbCustomerTwo.Location = new System.Drawing.Point(356, 31);
            this.cbCustomerTwo.Name = "cbCustomerTwo";
            this.cbCustomerTwo.Size = new System.Drawing.Size(297, 23);
            this.cbCustomerTwo.TabIndex = 5;
            this.cbCustomerTwo.SelectedIndexChanged += new System.EventHandler(this.cbCustomerTwo_SelectedIndexChanged);
            // 
            // lbTodaysCountHoliday
            // 
            this.lbTodaysCountHoliday.AutoSize = true;
            this.lbTodaysCountHoliday.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbTodaysCountHoliday.ForeColor = System.Drawing.Color.DarkRed;
            this.lbTodaysCountHoliday.Location = new System.Drawing.Point(40, 73);
            this.lbTodaysCountHoliday.Name = "lbTodaysCountHoliday";
            this.lbTodaysCountHoliday.Size = new System.Drawing.Size(341, 19);
            this.lbTodaysCountHoliday.TabIndex = 4;
            this.lbTodaysCountHoliday.Text = "一般配送: 0 假日配送: 0 *請在週一至週五完成出貨";
            // 
            // lbTodaysSummary
            // 
            this.lbTodaysSummary.AutoSize = true;
            this.lbTodaysSummary.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbTodaysSummary.Location = new System.Drawing.Point(33, 25);
            this.lbTodaysSummary.Name = "lbTodaysSummary";
            this.lbTodaysSummary.Size = new System.Drawing.Size(177, 30);
            this.lbTodaysSummary.TabIndex = 3;
            this.lbTodaysSummary.Text = "※今日總筆數：";
            // 
            // tabPageRearrange
            // 
            this.tabPageRearrange.AutoScroll = true;
            this.tabPageRearrange.Controls.Add(this.label10);
            this.tabPageRearrange.Controls.Add(this.btnCancelImport);
            this.tabPageRearrange.Controls.Add(this.btnConfirmImport);
            this.tabPageRearrange.Controls.Add(this.txtCustomFormatName);
            this.tabPageRearrange.Controls.Add(this.checkBoxSaveFormat);
            this.tabPageRearrange.Controls.Add(this.dataGridViewExcelData);
            this.tabPageRearrange.Controls.Add(this.lbRearrange);
            this.tabPageRearrange.Location = new System.Drawing.Point(4, 24);
            this.tabPageRearrange.Name = "tabPageRearrange";
            this.tabPageRearrange.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRearrange.Size = new System.Drawing.Size(1091, 731);
            this.tabPageRearrange.TabIndex = 2;
            this.tabPageRearrange.Text = "匯入託運單資料";
            this.tabPageRearrange.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DarkRed;
            this.label10.Location = new System.Drawing.Point(155, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 15);
            this.label10.TabIndex = 8;
            this.label10.Text = "(開始位置只可從第二行開始)";
            // 
            // btnCancelImport
            // 
            this.btnCancelImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCancelImport.FlatAppearance.BorderSize = 0;
            this.btnCancelImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelImport.ForeColor = System.Drawing.Color.White;
            this.btnCancelImport.Location = new System.Drawing.Point(259, 592);
            this.btnCancelImport.Name = "btnCancelImport";
            this.btnCancelImport.Size = new System.Drawing.Size(146, 41);
            this.btnCancelImport.TabIndex = 7;
            this.btnCancelImport.Text = "取消匯入";
            this.btnCancelImport.UseVisualStyleBackColor = false;
            this.btnCancelImport.Click += new System.EventHandler(this.btnCancelImport_Click);
            // 
            // btnConfirmImport
            // 
            this.btnConfirmImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnConfirmImport.FlatAppearance.BorderSize = 0;
            this.btnConfirmImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmImport.ForeColor = System.Drawing.Color.White;
            this.btnConfirmImport.Location = new System.Drawing.Point(58, 592);
            this.btnConfirmImport.Name = "btnConfirmImport";
            this.btnConfirmImport.Size = new System.Drawing.Size(146, 41);
            this.btnConfirmImport.TabIndex = 7;
            this.btnConfirmImport.Text = "確認匯入";
            this.btnConfirmImport.UseVisualStyleBackColor = false;
            this.btnConfirmImport.Click += new System.EventHandler(this.btnConfirmImport_Click);
            // 
            // txtCustomFormatName
            // 
            this.txtCustomFormatName.Location = new System.Drawing.Point(294, 531);
            this.txtCustomFormatName.Name = "txtCustomFormatName";
            this.txtCustomFormatName.PlaceholderText = "我的新格式之一";
            this.txtCustomFormatName.Size = new System.Drawing.Size(181, 23);
            this.txtCustomFormatName.TabIndex = 6;
            // 
            // checkBoxSaveFormat
            // 
            this.checkBoxSaveFormat.AutoSize = true;
            this.checkBoxSaveFormat.Location = new System.Drawing.Point(58, 533);
            this.checkBoxSaveFormat.Name = "checkBoxSaveFormat";
            this.checkBoxSaveFormat.Size = new System.Drawing.Size(230, 19);
            this.checkBoxSaveFormat.TabIndex = 5;
            this.checkBoxSaveFormat.Text = "把目前的欄位順序儲存成一種新的格式";
            this.checkBoxSaveFormat.UseVisualStyleBackColor = true;
            // 
            // dataGridViewExcelData
            // 
            this.dataGridViewExcelData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewExcelData.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridViewExcelData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewExcelData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.start,
            this.selectAll});
            this.dataGridViewExcelData.GridColor = System.Drawing.Color.White;
            this.dataGridViewExcelData.Location = new System.Drawing.Point(50, 85);
            this.dataGridViewExcelData.Name = "dataGridViewExcelData";
            this.dataGridViewExcelData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewExcelData.Size = new System.Drawing.Size(655, 422);
            this.dataGridViewExcelData.TabIndex = 4;
            this.dataGridViewExcelData.Text = "dgv";
            this.dataGridViewExcelData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewExcelData_CellClick);
            this.dataGridViewExcelData.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_HeaderMouseClick);
            // 
            // start
            // 
            this.start.HeaderText = "開始位置";
            this.start.Name = "start";
            this.start.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // selectAll
            // 
            this.selectAll.HeaderText = "全選/全不選";
            this.selectAll.Name = "selectAll";
            this.selectAll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // lbRearrange
            // 
            this.lbRearrange.AutoSize = true;
            this.lbRearrange.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbRearrange.Location = new System.Drawing.Point(50, 38);
            this.lbRearrange.Name = "lbRearrange";
            this.lbRearrange.Size = new System.Drawing.Size(85, 30);
            this.lbRearrange.TabIndex = 3;
            this.lbRearrange.Text = "排一排";
            // 
            // tabPageEdiReport
            // 
            this.tabPageEdiReport.AutoScroll = true;
            this.tabPageEdiReport.Controls.Add(this.dgvDeliveryRequest);
            this.tabPageEdiReport.Controls.Add(this.btnReportExport);
            this.tabPageEdiReport.Controls.Add(this.btnReportSearch);
            this.tabPageEdiReport.Controls.Add(this.panel1);
            this.tabPageEdiReport.Controls.Add(this.rbReverseDelivery);
            this.tabPageEdiReport.Controls.Add(this.rbDelivery);
            this.tabPageEdiReport.Controls.Add(this.lbType);
            this.tabPageEdiReport.Location = new System.Drawing.Point(4, 24);
            this.tabPageEdiReport.Name = "tabPageEdiReport";
            this.tabPageEdiReport.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEdiReport.Size = new System.Drawing.Size(1091, 731);
            this.tabPageEdiReport.TabIndex = 1;
            this.tabPageEdiReport.Text = "EDI訂單管理";
            this.tabPageEdiReport.UseVisualStyleBackColor = true;
            // 
            // dgvDeliveryRequest
            // 
            this.dgvDeliveryRequest.AllowUserToAddRows = false;
            this.dgvDeliveryRequest.AllowUserToDeleteRows = false;
            this.dgvDeliveryRequest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDeliveryRequest.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgvDeliveryRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliveryRequest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.originDate,
            this.shipDate,
            this.checkNumber,
            this.orderNumber,
            this.sendStation,
            this.sendCustomer,
            this.receiveContact,
            this.receiveTel,
            this.receiveCity,
            this.receiveArea,
            this.weight,
            this.peices,
            this.receiveAddress,
            this.areaArriveCode,
            this.arriveStation,
            this.subpoenaCategory,
            this.collectionMoney,
            this.note,
            this.ScanItem,
            this.ScanDate,
            this.RequestId});
            this.dgvDeliveryRequest.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgvDeliveryRequest.Location = new System.Drawing.Point(43, 363);
            this.dgvDeliveryRequest.Name = "dgvDeliveryRequest";
            this.dgvDeliveryRequest.Size = new System.Drawing.Size(1526, 381);
            this.dgvDeliveryRequest.TabIndex = 6;
            this.dgvDeliveryRequest.Text = "dataGridView1";
            // 
            // No
            // 
            this.No.DataPropertyName = "NO";
            this.No.HeaderText = "序號";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 52;
            // 
            // originDate
            // 
            this.originDate.DataPropertyName = "print_date";
            this.originDate.HeaderText = "發送日期";
            this.originDate.Name = "originDate";
            this.originDate.ReadOnly = true;
            this.originDate.Width = 63;
            // 
            // shipDate
            // 
            this.shipDate.DataPropertyName = "supplier_date";
            this.shipDate.HeaderText = "配送日期";
            this.shipDate.Name = "shipDate";
            this.shipDate.ReadOnly = true;
            this.shipDate.Width = 63;
            // 
            // checkNumber
            // 
            this.checkNumber.DataPropertyName = "check_number";
            this.checkNumber.HeaderText = "明細貨號";
            this.checkNumber.Name = "checkNumber";
            this.checkNumber.ReadOnly = true;
            this.checkNumber.Width = 63;
            // 
            // orderNumber
            // 
            this.orderNumber.DataPropertyName = "order_number";
            this.orderNumber.HeaderText = "訂單編號";
            this.orderNumber.Name = "orderNumber";
            this.orderNumber.ReadOnly = true;
            this.orderNumber.Width = 63;
            // 
            // sendStation
            // 
            this.sendStation.DataPropertyName = "send_station";
            this.sendStation.HeaderText = "發送站";
            this.sendStation.Name = "sendStation";
            this.sendStation.ReadOnly = true;
            this.sendStation.Width = 63;
            // 
            // sendCustomer
            // 
            this.sendCustomer.DataPropertyName = "send_contact";
            this.sendCustomer.HeaderText = "寄件人";
            this.sendCustomer.Name = "sendCustomer";
            this.sendCustomer.ReadOnly = true;
            this.sendCustomer.Width = 63;
            // 
            // receiveContact
            // 
            this.receiveContact.DataPropertyName = "receive_contact";
            this.receiveContact.HeaderText = "收件人";
            this.receiveContact.Name = "receiveContact";
            this.receiveContact.ReadOnly = true;
            this.receiveContact.Width = 63;
            // 
            // receiveTel
            // 
            this.receiveTel.DataPropertyName = "receiveTel";
            this.receiveTel.HeaderText = "收貨人電話號碼";
            this.receiveTel.Name = "receiveTel";
            this.receiveTel.ReadOnly = true;
            this.receiveTel.Width = 85;
            // 
            // receiveCity
            // 
            this.receiveCity.DataPropertyName = "receive_city";
            this.receiveCity.HeaderText = "配送縣市";
            this.receiveCity.Name = "receiveCity";
            this.receiveCity.ReadOnly = true;
            this.receiveCity.Width = 63;
            // 
            // receiveArea
            // 
            this.receiveArea.DataPropertyName = "receive_area";
            this.receiveArea.HeaderText = "配送區域";
            this.receiveArea.Name = "receiveArea";
            this.receiveArea.ReadOnly = true;
            this.receiveArea.Width = 63;
            // 
            // weight
            // 
            this.weight.DataPropertyName = "cbmWeight";
            this.weight.HeaderText = "重量";
            this.weight.Name = "weight";
            this.weight.ReadOnly = true;
            this.weight.Width = 52;
            // 
            // peices
            // 
            this.peices.DataPropertyName = "pieces";
            this.peices.HeaderText = "件數";
            this.peices.Name = "peices";
            this.peices.ReadOnly = true;
            this.peices.Width = 52;
            // 
            // receiveAddress
            // 
            this.receiveAddress.DataPropertyName = "receiveAddress";
            this.receiveAddress.HeaderText = "收件人地址";
            this.receiveAddress.Name = "receiveAddress";
            this.receiveAddress.ReadOnly = true;
            this.receiveAddress.Width = 74;
            // 
            // areaArriveCode
            // 
            this.areaArriveCode.DataPropertyName = "area_arrive_code";
            this.areaArriveCode.HeaderText = "站所代碼";
            this.areaArriveCode.Name = "areaArriveCode";
            this.areaArriveCode.ReadOnly = true;
            this.areaArriveCode.Width = 63;
            // 
            // arriveStation
            // 
            this.arriveStation.DataPropertyName = "receive_station";
            this.arriveStation.HeaderText = "站所名稱";
            this.arriveStation.Name = "arriveStation";
            this.arriveStation.ReadOnly = true;
            this.arriveStation.Width = 63;
            // 
            // subpoenaCategory
            // 
            this.subpoenaCategory.DataPropertyName = "code_name";
            this.subpoenaCategory.HeaderText = "傳票區分";
            this.subpoenaCategory.Name = "subpoenaCategory";
            this.subpoenaCategory.ReadOnly = true;
            this.subpoenaCategory.Width = 63;
            // 
            // collectionMoney
            // 
            this.collectionMoney.DataPropertyName = "collection_money";
            this.collectionMoney.HeaderText = "代收金額";
            this.collectionMoney.Name = "collectionMoney";
            this.collectionMoney.ReadOnly = true;
            this.collectionMoney.Width = 63;
            // 
            // note
            // 
            this.note.DataPropertyName = "invoice_desc";
            this.note.HeaderText = "備註";
            this.note.Name = "note";
            this.note.ReadOnly = true;
            this.note.Width = 52;
            // 
            // ScanItem
            // 
            this.ScanItem.DataPropertyName = "arrive_state";
            this.ScanItem.HeaderText = "貨態";
            this.ScanItem.Name = "ScanItem";
            this.ScanItem.ReadOnly = true;
            this.ScanItem.Width = 52;
            // 
            // ScanDate
            // 
            this.ScanDate.DataPropertyName = "newest_scandate";
            this.ScanDate.HeaderText = "掃讀日期";
            this.ScanDate.Name = "ScanDate";
            this.ScanDate.ReadOnly = true;
            this.ScanDate.Width = 63;
            // 
            // RequestId
            // 
            this.RequestId.DataPropertyName = "request_id";
            this.RequestId.HeaderText = "requestId";
            this.RequestId.Name = "RequestId";
            this.RequestId.ReadOnly = true;
            this.RequestId.Visible = false;
            this.RequestId.Width = 85;
            // 
            // btnReportExport
            // 
            this.btnReportExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnReportExport.FlatAppearance.BorderSize = 0;
            this.btnReportExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportExport.ForeColor = System.Drawing.Color.White;
            this.btnReportExport.Location = new System.Drawing.Point(184, 308);
            this.btnReportExport.Name = "btnReportExport";
            this.btnReportExport.Size = new System.Drawing.Size(85, 34);
            this.btnReportExport.TabIndex = 5;
            this.btnReportExport.Text = "匯出";
            this.btnReportExport.UseVisualStyleBackColor = false;
            this.btnReportExport.Click += new System.EventHandler(this.btnReportExport_Click);
            // 
            // btnReportSearch
            // 
            this.btnReportSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnReportSearch.FlatAppearance.BorderSize = 0;
            this.btnReportSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportSearch.ForeColor = System.Drawing.Color.White;
            this.btnReportSearch.Location = new System.Drawing.Point(43, 308);
            this.btnReportSearch.Name = "btnReportSearch";
            this.btnReportSearch.Size = new System.Drawing.Size(96, 34);
            this.btnReportSearch.TabIndex = 4;
            this.btnReportSearch.Text = "查詢";
            this.btnReportSearch.UseVisualStyleBackColor = false;
            this.btnReportSearch.Click += new System.EventHandler(this.btnReportSearch_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtCheckNum);
            this.panel1.Controls.Add(this.cbScanItem);
            this.panel1.Controls.Add(this.cbCustomerFour);
            this.panel1.Controls.Add(this.dtPrintDateEnd);
            this.panel1.Controls.Add(this.dtPrintDateStart);
            this.panel1.Controls.Add(this.lbReportFilterShipdate);
            this.panel1.Controls.Add(this.lbReportFilterCustomerCode);
            this.panel1.Controls.Add(this.lbReportFilterScanItem);
            this.panel1.Controls.Add(this.lbReportFilterCheckNumber);
            this.panel1.Controls.Add(this.lbReportTotalCountValue);
            this.panel1.Controls.Add(this.lbStationValue);
            this.panel1.Controls.Add(this.lbTotalCount);
            this.panel1.Controls.Add(this.lbStation);
            this.panel1.Location = new System.Drawing.Point(43, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 204);
            this.panel1.TabIndex = 3;
            // 
            // txtCheckNum
            // 
            this.txtCheckNum.Location = new System.Drawing.Point(108, 151);
            this.txtCheckNum.Name = "txtCheckNum";
            this.txtCheckNum.Size = new System.Drawing.Size(200, 23);
            this.txtCheckNum.TabIndex = 7;
            // 
            // cbScanItem
            // 
            this.cbScanItem.FormattingEnabled = true;
            this.cbScanItem.Location = new System.Drawing.Point(108, 118);
            this.cbScanItem.Name = "cbScanItem";
            this.cbScanItem.Size = new System.Drawing.Size(200, 23);
            this.cbScanItem.TabIndex = 6;
            // 
            // cbCustomerFour
            // 
            this.cbCustomerFour.FormattingEnabled = true;
            this.cbCustomerFour.Location = new System.Drawing.Point(108, 88);
            this.cbCustomerFour.Name = "cbCustomerFour";
            this.cbCustomerFour.Size = new System.Drawing.Size(200, 23);
            this.cbCustomerFour.TabIndex = 5;
            this.cbCustomerFour.SelectedIndexChanged += new System.EventHandler(this.cbCustomer_SelectedIndexChanged);
            // 
            // dtPrintDateEnd
            // 
            this.dtPrintDateEnd.Location = new System.Drawing.Point(355, 59);
            this.dtPrintDateEnd.Name = "dtPrintDateEnd";
            this.dtPrintDateEnd.Size = new System.Drawing.Size(200, 23);
            this.dtPrintDateEnd.TabIndex = 4;
            // 
            // dtPrintDateStart
            // 
            this.dtPrintDateStart.Location = new System.Drawing.Point(108, 59);
            this.dtPrintDateStart.Name = "dtPrintDateStart";
            this.dtPrintDateStart.Size = new System.Drawing.Size(200, 23);
            this.dtPrintDateStart.TabIndex = 3;
            // 
            // lbReportFilterShipdate
            // 
            this.lbReportFilterShipdate.AutoSize = true;
            this.lbReportFilterShipdate.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbReportFilterShipdate.Location = new System.Drawing.Point(7, 59);
            this.lbReportFilterShipdate.Name = "lbReportFilterShipdate";
            this.lbReportFilterShipdate.Size = new System.Drawing.Size(89, 20);
            this.lbReportFilterShipdate.TabIndex = 1;
            this.lbReportFilterShipdate.Text = "發送日期：";
            // 
            // lbReportFilterCustomerCode
            // 
            this.lbReportFilterCustomerCode.AutoSize = true;
            this.lbReportFilterCustomerCode.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbReportFilterCustomerCode.Location = new System.Drawing.Point(7, 91);
            this.lbReportFilterCustomerCode.Name = "lbReportFilterCustomerCode";
            this.lbReportFilterCustomerCode.Size = new System.Drawing.Size(89, 20);
            this.lbReportFilterCustomerCode.TabIndex = 1;
            this.lbReportFilterCustomerCode.Text = "選擇客代：";
            // 
            // lbReportFilterScanItem
            // 
            this.lbReportFilterScanItem.AutoSize = true;
            this.lbReportFilterScanItem.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbReportFilterScanItem.Location = new System.Drawing.Point(39, 121);
            this.lbReportFilterScanItem.Name = "lbReportFilterScanItem";
            this.lbReportFilterScanItem.Size = new System.Drawing.Size(57, 20);
            this.lbReportFilterScanItem.TabIndex = 1;
            this.lbReportFilterScanItem.Text = "貨態：";
            // 
            // lbReportFilterCheckNumber
            // 
            this.lbReportFilterCheckNumber.AutoSize = true;
            this.lbReportFilterCheckNumber.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbReportFilterCheckNumber.Location = new System.Drawing.Point(39, 151);
            this.lbReportFilterCheckNumber.Name = "lbReportFilterCheckNumber";
            this.lbReportFilterCheckNumber.Size = new System.Drawing.Size(57, 20);
            this.lbReportFilterCheckNumber.TabIndex = 1;
            this.lbReportFilterCheckNumber.Text = "貨號：";
            // 
            // lbReportTotalCountValue
            // 
            this.lbReportTotalCountValue.AutoSize = true;
            this.lbReportTotalCountValue.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbReportTotalCountValue.ForeColor = System.Drawing.Color.DarkRed;
            this.lbReportTotalCountValue.Location = new System.Drawing.Point(280, 29);
            this.lbReportTotalCountValue.Name = "lbReportTotalCountValue";
            this.lbReportTotalCountValue.Size = new System.Drawing.Size(19, 20);
            this.lbReportTotalCountValue.TabIndex = 1;
            this.lbReportTotalCountValue.Text = "0";
            // 
            // lbStationValue
            // 
            this.lbStationValue.AutoSize = true;
            this.lbStationValue.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbStationValue.ForeColor = System.Drawing.Color.DarkRed;
            this.lbStationValue.Location = new System.Drawing.Point(108, 29);
            this.lbStationValue.Name = "lbStationValue";
            this.lbStationValue.Size = new System.Drawing.Size(0, 20);
            this.lbStationValue.TabIndex = 2;
            // 
            // lbTotalCount
            // 
            this.lbTotalCount.AutoSize = true;
            this.lbTotalCount.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbTotalCount.Location = new System.Drawing.Point(201, 29);
            this.lbTotalCount.Name = "lbTotalCount";
            this.lbTotalCount.Size = new System.Drawing.Size(73, 20);
            this.lbTotalCount.TabIndex = 1;
            this.lbTotalCount.Text = "總筆數：";
            // 
            // lbStation
            // 
            this.lbStation.AutoSize = true;
            this.lbStation.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbStation.Location = new System.Drawing.Point(39, 29);
            this.lbStation.Name = "lbStation";
            this.lbStation.Size = new System.Drawing.Size(57, 20);
            this.lbStation.TabIndex = 0;
            this.lbStation.Text = "站所：";
            // 
            // rbReverseDelivery
            // 
            this.rbReverseDelivery.AutoSize = true;
            this.rbReverseDelivery.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbReverseDelivery.Location = new System.Drawing.Point(244, 28);
            this.rbReverseDelivery.Name = "rbReverseDelivery";
            this.rbReverseDelivery.Size = new System.Drawing.Size(72, 23);
            this.rbReverseDelivery.TabIndex = 2;
            this.rbReverseDelivery.Text = "逆物流";
            this.rbReverseDelivery.UseVisualStyleBackColor = true;
            // 
            // rbDelivery
            // 
            this.rbDelivery.AutoSize = true;
            this.rbDelivery.Checked = true;
            this.rbDelivery.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbDelivery.Location = new System.Drawing.Point(151, 28);
            this.rbDelivery.Name = "rbDelivery";
            this.rbDelivery.Size = new System.Drawing.Size(72, 23);
            this.rbDelivery.TabIndex = 1;
            this.rbDelivery.TabStop = true;
            this.rbDelivery.Text = "正物流";
            this.rbDelivery.UseVisualStyleBackColor = true;
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbType.Location = new System.Drawing.Point(50, 31);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(89, 20);
            this.lbType.TabIndex = 0;
            this.lbType.Text = "發送類別：";
            // 
            // tabPageNotSuccess
            // 
            this.tabPageNotSuccess.AutoScroll = true;
            this.tabPageNotSuccess.Controls.Add(this.lbNotSuccessCountVal);
            this.tabPageNotSuccess.Controls.Add(this.btnRemoveFailedData);
            this.tabPageNotSuccess.Controls.Add(this.btnUploadAgain);
            this.tabPageNotSuccess.Controls.Add(this.label2);
            this.tabPageNotSuccess.Controls.Add(this.dataGridViewFailedRecord);
            this.tabPageNotSuccess.Controls.Add(this.lbNotSuccessCount);
            this.tabPageNotSuccess.Location = new System.Drawing.Point(4, 24);
            this.tabPageNotSuccess.Name = "tabPageNotSuccess";
            this.tabPageNotSuccess.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNotSuccess.Size = new System.Drawing.Size(1091, 731);
            this.tabPageNotSuccess.TabIndex = 3;
            this.tabPageNotSuccess.Text = "匯入託運單資料";
            this.tabPageNotSuccess.UseVisualStyleBackColor = true;
            // 
            // lbNotSuccessCountVal
            // 
            this.lbNotSuccessCountVal.AutoSize = true;
            this.lbNotSuccessCountVal.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbNotSuccessCountVal.ForeColor = System.Drawing.Color.DarkRed;
            this.lbNotSuccessCountVal.Location = new System.Drawing.Point(213, 44);
            this.lbNotSuccessCountVal.Name = "lbNotSuccessCountVal";
            this.lbNotSuccessCountVal.Size = new System.Drawing.Size(21, 24);
            this.lbNotSuccessCountVal.TabIndex = 0;
            this.lbNotSuccessCountVal.Text = "3";
            // 
            // btnRemoveFailedData
            // 
            this.btnRemoveFailedData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnRemoveFailedData.FlatAppearance.BorderSize = 0;
            this.btnRemoveFailedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveFailedData.ForeColor = System.Drawing.Color.White;
            this.btnRemoveFailedData.Location = new System.Drawing.Point(263, 570);
            this.btnRemoveFailedData.Name = "btnRemoveFailedData";
            this.btnRemoveFailedData.Size = new System.Drawing.Size(131, 48);
            this.btnRemoveFailedData.TabIndex = 2;
            this.btnRemoveFailedData.Text = "清除未上傳資料";
            this.btnRemoveFailedData.UseVisualStyleBackColor = false;
            this.btnRemoveFailedData.Click += new System.EventHandler(this.btnRemoveFailedData_Click);
            // 
            // btnUploadAgain
            // 
            this.btnUploadAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnUploadAgain.FlatAppearance.BorderSize = 0;
            this.btnUploadAgain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUploadAgain.ForeColor = System.Drawing.Color.White;
            this.btnUploadAgain.Location = new System.Drawing.Point(69, 570);
            this.btnUploadAgain.Name = "btnUploadAgain";
            this.btnUploadAgain.Size = new System.Drawing.Size(131, 48);
            this.btnUploadAgain.TabIndex = 2;
            this.btnUploadAgain.Text = "重新上傳";
            this.btnUploadAgain.UseVisualStyleBackColor = false;
            this.btnUploadAgain.Click += new System.EventHandler(this.btnUploadAgain_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(69, 521);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(399, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "*欄位顯示黃色區塊，請點選「修改」進行調整";
            // 
            // dataGridViewFailedRecord
            // 
            this.dataGridViewFailedRecord.AllowUserToAddRows = false;
            this.dataGridViewFailedRecord.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridViewFailedRecord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFailedRecord.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridViewFailedRecord.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewFailedRecord.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridViewFailedRecord.Location = new System.Drawing.Point(69, 101);
            this.dataGridViewFailedRecord.Name = "dataGridViewFailedRecord";
            this.dataGridViewFailedRecord.Size = new System.Drawing.Size(883, 369);
            this.dataGridViewFailedRecord.TabIndex = 1;
            this.dataGridViewFailedRecord.Text = "dataGridView2";
            this.dataGridViewFailedRecord.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_HeaderMouseClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "全選/全不選";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "訂單編號";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "收件人姓名";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "收件人電話";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "收件人地址";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "件數";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "重量";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // lbNotSuccessCount
            // 
            this.lbNotSuccessCount.AutoSize = true;
            this.lbNotSuccessCount.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbNotSuccessCount.Location = new System.Drawing.Point(69, 44);
            this.lbNotSuccessCount.Name = "lbNotSuccessCount";
            this.lbNotSuccessCount.Size = new System.Drawing.Size(202, 24);
            this.lbNotSuccessCount.TabIndex = 0;
            this.lbNotSuccessCount.Text = "未上傳成功筆數        筆";
            // 
            // tabPagePickupRequest
            // 
            this.tabPagePickupRequest.AutoScroll = true;
            this.tabPagePickupRequest.Controls.Add(this.numericPickupCount);
            this.tabPagePickupRequest.Controls.Add(this.cbReceiveTime);
            this.tabPagePickupRequest.Controls.Add(this.cbPickupType);
            this.tabPagePickupRequest.Controls.Add(this.cbCarrier);
            this.tabPagePickupRequest.Controls.Add(this.btnSendPickupRequest);
            this.tabPagePickupRequest.Controls.Add(this.txtPickUpMemo);
            this.tabPagePickupRequest.Controls.Add(this.label6);
            this.tabPagePickupRequest.Controls.Add(this.label5);
            this.tabPagePickupRequest.Controls.Add(this.label4);
            this.tabPagePickupRequest.Controls.Add(this.label1);
            this.tabPagePickupRequest.Controls.Add(this.lbTodayShipCount);
            this.tabPagePickupRequest.Location = new System.Drawing.Point(4, 24);
            this.tabPagePickupRequest.Name = "tabPagePickupRequest";
            this.tabPagePickupRequest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePickupRequest.Size = new System.Drawing.Size(1091, 731);
            this.tabPagePickupRequest.TabIndex = 4;
            this.tabPagePickupRequest.Text = "派員收件";
            this.tabPagePickupRequest.UseVisualStyleBackColor = true;
            // 
            // numericPickupCount
            // 
            this.numericPickupCount.Location = new System.Drawing.Point(189, 81);
            this.numericPickupCount.Name = "numericPickupCount";
            this.numericPickupCount.Size = new System.Drawing.Size(120, 23);
            this.numericPickupCount.TabIndex = 6;
            // 
            // cbReceiveTime
            // 
            this.cbReceiveTime.FormattingEnabled = true;
            this.cbReceiveTime.Location = new System.Drawing.Point(189, 223);
            this.cbReceiveTime.Name = "cbReceiveTime";
            this.cbReceiveTime.Size = new System.Drawing.Size(121, 23);
            this.cbReceiveTime.TabIndex = 5;
            // 
            // cbPickupType
            // 
            this.cbPickupType.FormattingEnabled = true;
            this.cbPickupType.Location = new System.Drawing.Point(189, 178);
            this.cbPickupType.Name = "cbPickupType";
            this.cbPickupType.Size = new System.Drawing.Size(121, 23);
            this.cbPickupType.TabIndex = 5;
            // 
            // cbCarrier
            // 
            this.cbCarrier.FormattingEnabled = true;
            this.cbCarrier.Location = new System.Drawing.Point(189, 133);
            this.cbCarrier.Name = "cbCarrier";
            this.cbCarrier.Size = new System.Drawing.Size(121, 23);
            this.cbCarrier.TabIndex = 4;
            // 
            // btnSendPickupRequest
            // 
            this.btnSendPickupRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.btnSendPickupRequest.FlatAppearance.BorderSize = 0;
            this.btnSendPickupRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendPickupRequest.ForeColor = System.Drawing.Color.White;
            this.btnSendPickupRequest.Location = new System.Drawing.Point(78, 496);
            this.btnSendPickupRequest.Name = "btnSendPickupRequest";
            this.btnSendPickupRequest.Size = new System.Drawing.Size(104, 39);
            this.btnSendPickupRequest.TabIndex = 2;
            this.btnSendPickupRequest.Text = "派員收件";
            this.btnSendPickupRequest.UseVisualStyleBackColor = false;
            this.btnSendPickupRequest.Click += new System.EventHandler(this.btnSendPickupRequest_Click);
            // 
            // txtPickUpMemo
            // 
            this.txtPickUpMemo.Location = new System.Drawing.Point(78, 305);
            this.txtPickUpMemo.Multiline = true;
            this.txtPickUpMemo.Name = "txtPickUpMemo";
            this.txtPickUpMemo.Size = new System.Drawing.Size(402, 160);
            this.txtPickUpMemo.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(78, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "承運載具：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(78, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "貨件規格：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(78, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "收件時間：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(78, 269);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "提醒事項";
            // 
            // lbTodayShipCount
            // 
            this.lbTodayShipCount.AutoSize = true;
            this.lbTodayShipCount.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbTodayShipCount.Location = new System.Drawing.Point(48, 85);
            this.lbTodayShipCount.Name = "lbTodayShipCount";
            this.lbTodayShipCount.Size = new System.Drawing.Size(114, 19);
            this.lbTodayShipCount.TabIndex = 0;
            this.lbTodayShipCount.Text = "今日出貨件數：";
            // 
            // LeftPanel
            // 
            this.LeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(193)))), ((int)(((byte)(221)))));
            this.LeftPanel.Controls.Add(this.picLogo);
            this.LeftPanel.Controls.Add(this.panelServiceDropDown);
            this.LeftPanel.Controls.Add(this.panelReportDropDown);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(227, 809);
            this.LeftPanel.TabIndex = 3;
            // 
            // panelServiceDropDown
            // 
            this.panelServiceDropDown.Controls.Add(this.btnTabImport);
            this.panelServiceDropDown.Controls.Add(this.btnServiceDropDown);
            this.panelServiceDropDown.Location = new System.Drawing.Point(3, 124);
            this.panelServiceDropDown.MaximumSize = new System.Drawing.Size(224, 122);
            this.panelServiceDropDown.MinimumSize = new System.Drawing.Size(224, 64);
            this.panelServiceDropDown.Name = "panelServiceDropDown";
            this.panelServiceDropDown.Size = new System.Drawing.Size(224, 64);
            this.panelServiceDropDown.TabIndex = 1;
            // 
            // btnTabImport
            // 
            this.btnTabImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(213)))), ((int)(((byte)(255)))));
            this.btnTabImport.FlatAppearance.BorderSize = 0;
            this.btnTabImport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btnTabImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabImport.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTabImport.ForeColor = System.Drawing.Color.Black;
            this.btnTabImport.Location = new System.Drawing.Point(0, 65);
            this.btnTabImport.Name = "btnTabImport";
            this.btnTabImport.Size = new System.Drawing.Size(224, 53);
            this.btnTabImport.TabIndex = 0;
            this.btnTabImport.Text = "匯入託運單資料";
            this.btnTabImport.UseVisualStyleBackColor = false;
            this.btnTabImport.Click += new System.EventHandler(this.btnTabImport_Click);
            // 
            // btnServiceDropDown
            // 
            this.btnServiceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(187)))));
            this.btnServiceDropDown.FlatAppearance.BorderSize = 0;
            this.btnServiceDropDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServiceDropDown.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnServiceDropDown.ForeColor = System.Drawing.Color.White;
            this.btnServiceDropDown.Location = new System.Drawing.Point(0, 6);
            this.btnServiceDropDown.Name = "btnServiceDropDown";
            this.btnServiceDropDown.Size = new System.Drawing.Size(224, 53);
            this.btnServiceDropDown.TabIndex = 0;
            this.btnServiceDropDown.Text = "託運服務";
            this.btnServiceDropDown.UseVisualStyleBackColor = false;
            this.btnServiceDropDown.Click += new System.EventHandler(this.btnServiceDropDown_Click);
            // 
            // panelReportDropDown
            // 
            this.panelReportDropDown.Controls.Add(this.btnTabEdiReport);
            this.panelReportDropDown.Controls.Add(this.btnReportDropDown);
            this.panelReportDropDown.Location = new System.Drawing.Point(3, 194);
            this.panelReportDropDown.MaximumSize = new System.Drawing.Size(224, 122);
            this.panelReportDropDown.MinimumSize = new System.Drawing.Size(224, 64);
            this.panelReportDropDown.Name = "panelReportDropDown";
            this.panelReportDropDown.Size = new System.Drawing.Size(224, 64);
            this.panelReportDropDown.TabIndex = 1;
            // 
            // btnTabEdiReport
            // 
            this.btnTabEdiReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(213)))), ((int)(((byte)(255)))));
            this.btnTabEdiReport.FlatAppearance.BorderSize = 0;
            this.btnTabEdiReport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btnTabEdiReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabEdiReport.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTabEdiReport.ForeColor = System.Drawing.Color.Black;
            this.btnTabEdiReport.Location = new System.Drawing.Point(0, 65);
            this.btnTabEdiReport.Name = "btnTabEdiReport";
            this.btnTabEdiReport.Size = new System.Drawing.Size(224, 53);
            this.btnTabEdiReport.TabIndex = 0;
            this.btnTabEdiReport.Text = "EDI訂單管理";
            this.btnTabEdiReport.UseVisualStyleBackColor = false;
            this.btnTabEdiReport.Click += new System.EventHandler(this.btnTabEdiReport_Click);
            // 
            // btnReportDropDown
            // 
            this.btnReportDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(187)))));
            this.btnReportDropDown.FlatAppearance.BorderSize = 0;
            this.btnReportDropDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportDropDown.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnReportDropDown.ForeColor = System.Drawing.Color.White;
            this.btnReportDropDown.Location = new System.Drawing.Point(0, 6);
            this.btnReportDropDown.Name = "btnReportDropDown";
            this.btnReportDropDown.Size = new System.Drawing.Size(224, 53);
            this.btnReportDropDown.TabIndex = 0;
            this.btnReportDropDown.Text = "報表管理";
            this.btnReportDropDown.UseVisualStyleBackColor = false;
            this.btnReportDropDown.Click += new System.EventHandler(this.btnReportDropDown_Click);
            // 
            // timerServiceDropDown
            // 
            this.timerServiceDropDown.Interval = 15;
            this.timerServiceDropDown.Tick += new System.EventHandler(this.timerServiceDropDown_Tick);
            // 
            // timerReportDropDown
            // 
            this.timerReportDropDown.Interval = 15;
            this.timerReportDropDown.Tick += new System.EventHandler(this.timerReportDropDown_Tick);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1068, 690);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft JhengHei UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(562, 410);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 30);
            this.label8.TabIndex = 3;
            this.label8.Text = "5. 出貨";
            // 
            // Main
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1331, 809);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.LeftPanel);
            this.Controls.Add(this.tabControlMain);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "FSE_EDI";
            this.Load += new System.EventHandler(this.Main_Load);
            this.panelUpload.ResumeLayout(false);
            this.panelUpload.PerformLayout();
            this.panelDownload.ResumeLayout(false);
            this.panelDownload.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panelTitleText.ResumeLayout(false);
            this.panelTitleText.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogout)).EndInit();
            this.panelInfromation.ResumeLayout(false);
            this.panelInfromation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageUpload.ResumeLayout(false);
            this.tabPageUpload.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTodaysTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrintEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrintStart)).EndInit();
            this.tabPageRearrange.ResumeLayout(false);
            this.tabPageRearrange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcelData)).EndInit();
            this.tabPageEdiReport.ResumeLayout(false);
            this.tabPageEdiReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryRequest)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPageNotSuccess.ResumeLayout(false);
            this.tabPageNotSuccess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFailedRecord)).EndInit();
            this.tabPagePickupRequest.ResumeLayout(false);
            this.tabPagePickupRequest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPickupCount)).EndInit();
            this.LeftPanel.ResumeLayout(false);
            this.panelServiceDropDown.ResumeLayout(false);
            this.panelReportDropDown.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelTitleText;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label lbSelectedPage;
        private RoundedButton btnDownloadFile;
        private RoundedButton btnDownload123File;
        private System.Windows.Forms.Panel panelDownload;
        private System.Windows.Forms.Label lbDownloadWarning;
        private System.Windows.Forms.Panel panelUpload;
        private System.Windows.Forms.Label lbUploadTitle;
        private System.Windows.Forms.Label lbDownloadTitle;
        private System.Windows.Forms.ComboBox cbCustomer;
        private System.Windows.Forms.TextBox txtSelectedFile;
        private System.Windows.Forms.Label lbFormat;
        private System.Windows.Forms.Label lbFormatCategory;
        private System.Windows.Forms.Label lbShipDate;
        private System.Windows.Forms.Label lbCusotmer;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.RadioButton rbCustomFormat;
        private System.Windows.Forms.RadioButton rbSolidFormat;
        private System.Windows.Forms.DateTimePicker dateOrigin;
        private RoundedButton btnUpload;
        private RoundedButton btnSelectFile;
        private System.Windows.Forms.TextBox txtFeedBack;
        private System.Windows.Forms.Label lbFileNotSelect;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageUpload;
        private System.Windows.Forms.TabPage tabPageEdiReport;
        private System.Windows.Forms.FlowLayoutPanel LeftPanel;
        private System.Windows.Forms.Panel panelServiceDropDown;
        private System.Windows.Forms.Button btnTabImport;
        private System.Windows.Forms.Button btnServiceDropDown;
        private System.Windows.Forms.Button btnEdiReport;
        private System.Windows.Forms.Button btnReportDropDown;
        private System.Windows.Forms.Panel panelReportDropDown;
        private System.Windows.Forms.Timer timerServiceDropDown;
        private System.Windows.Forms.Timer timerReportDropDown;
        private System.Windows.Forms.Button btnTabEdiReport;
        private System.Windows.Forms.TabPage tabPageRearrange;
        private System.Windows.Forms.DataGridView dataGridViewExcelData;
        private System.Windows.Forms.Label lbRearrange;
        private System.Windows.Forms.Label lbGreeting;
        private System.Windows.Forms.Panel panelInfromation;
        private System.Windows.Forms.Label lbMessageContent;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Button btnReportExport;
        private System.Windows.Forms.Button btnReportSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCheckNum;
        private System.Windows.Forms.ComboBox cbScanItem;
        private System.Windows.Forms.ComboBox cbCustomerFour;
        private System.Windows.Forms.DateTimePicker dtPrintDateEnd;
        private System.Windows.Forms.DateTimePicker dtPrintDateStart;
        private System.Windows.Forms.Label lbReportFilterShipdate;
        private System.Windows.Forms.Label lbReportFilterCustomerCode;
        private System.Windows.Forms.Label lbReportFilterScanItem;
        private System.Windows.Forms.Label lbReportFilterCheckNumber;
        private System.Windows.Forms.Label lbReportTotalCountValue;
        private System.Windows.Forms.Label lbStationValue;
        private System.Windows.Forms.Label lbTotalCount;
        private System.Windows.Forms.Label lbStation;
        private System.Windows.Forms.RadioButton rbReverseDelivery;
        private System.Windows.Forms.RadioButton rbDelivery;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.DataGridView dgvDeliveryRequest;
        private System.Windows.Forms.Button btnCancelImport;
        private System.Windows.Forms.Button btnConfirmImport;
        private System.Windows.Forms.TextBox txtCustomFormatName;
        private System.Windows.Forms.CheckBox checkBoxSaveFormat;
        private System.Windows.Forms.DataGridViewImageColumn start;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectAll;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPageNotSuccess;
        private System.Windows.Forms.Button btnRemoveFailedData;
        private System.Windows.Forms.Button btnUploadAgain;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewFailedRecord;
        private System.Windows.Forms.Label lbNotSuccessCount;
        private System.Windows.Forms.ComboBox cbCustomerAt;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private RoundedButton btnTryAgain;
        private System.Windows.Forms.Label lbNotSuccessCountVal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvTodaysTotal;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.NumericUpDown numericPrintEnd;
        private System.Windows.Forms.NumericUpDown numericPrintStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbNotSelectAll;
        private System.Windows.Forms.RadioButton rbSelectAll;
        private System.Windows.Forms.ComboBox cbCustomerTwo;
        private System.Windows.Forms.Label lbTodaysCountHoliday;
        private System.Windows.Forms.Label lbTodaysSummary;
        private System.Windows.Forms.Label lbPrint;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnPrintLabel;
        private System.Windows.Forms.Label lbPrintStartPos;
        private System.Windows.Forms.ComboBox cbPaperFormat;
        private System.Windows.Forms.Label lbPaperFormat;
        private System.Windows.Forms.CheckBox cbPrintShowSender;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbPos1;
        private System.Windows.Forms.RadioButton rbPos2;
        private System.Windows.Forms.RadioButton rbPos3;
        private System.Windows.Forms.RadioButton rbPos5;
        private System.Windows.Forms.RadioButton rbPos6;
        private System.Windows.Forms.RadioButton rbPos4;
        private System.Windows.Forms.TextBox txtPickUpMemo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTodayShipCount;
        private System.Windows.Forms.Button btnSendPickupRequest;
        private System.Windows.Forms.ComboBox cbReceiveTime;
        private System.Windows.Forms.ComboBox cbPickupType;
        private System.Windows.Forms.ComboBox cbCarrier;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGotoPickupRequest;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbCustomerThree;
        private System.Windows.Forms.TabPage tabPagePickupRequest;
        private System.Windows.Forms.NumericUpDown numericPickupCount;
        private System.Windows.Forms.PictureBox picLogout;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn originDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendStation;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveContact;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn peices;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn areaArriveCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn arriveStation;
        private System.Windows.Forms.DataGridViewTextBoxColumn subpoenaCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn collectionMoney;
        private System.Windows.Forms.DataGridViewTextBoxColumn note;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScanItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScanDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestId;
        private System.Windows.Forms.Button btnDownloadRequestExcel;
        private System.Windows.Forms.ComboBox cbFormatDownload;
        private System.Windows.Forms.Button btnDownloadCustomFormat;
        private System.Windows.Forms.Button btnDeleteCustomFormat;
        private System.Windows.Forms.Label label10;
    }
}