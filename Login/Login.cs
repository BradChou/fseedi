﻿using Common;
using EdiMain.Model;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace Login
{
    public partial class Login : Form
    {
        UserEntity User { get; set; }

        public Login()
        {
            InitializeComponent();
            User = new UserEntity();
        }

        private void StartUploadSystem()
        {
            //執行新的印標程式
            string InputJson = WebUtility.UrlEncode(JsonConvert.SerializeObject(User));

            string szExeFile = ConfigurationManager.AppSettings["UpdaterExe"];

            string exeFullPath = Path.Combine(Application.StartupPath, szExeFile);

            System.Diagnostics.Process.Start(exeFullPath, InputJson);

            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (tbAccount.Text.Length == 0 || tbPassword.Text.Length == 0)
            {
                MessageBox.Show("請輸入帳號密碼");
                return;
            }

            string token = LoginMethod.GetToken(tbAccount.Text, tbPassword.Text);

            if (token == null)
            {
                MessageBox.Show("帳密錯誤!");
                return;
            }

            User.AccountCode = tbAccount.Text;
            User.Token = token;

            StartUploadSystem();
        }

        private void lbForgetPW_Click(object sender, EventArgs e)
        {
            using(ForgetPasswordDialog dialog = new ForgetPasswordDialog())
            {
                dialog.Owner = this;
                var result = dialog.ShowDialog();
                
                if(result == DialogResult.OK)
                {
                    string account = dialog.Account;
                    string email = dialog.Email;

                    bool isAccountCorrect = CheckAccount(account);

                    if (!isAccountCorrect)
                    {
                        MessageBox.Show("您輸入的帳號錯誤");
                        return;
                    }

                    bool isEmailCorrect = CheckEmail(account, email);

                    if (isEmailCorrect)
                    {
                        string password = GenerateNewPassword();

                        MD5 md5 = MD5.Create();
                        string md5Password = GetMd5Hash(md5, password);

                        UpdateDbPassword(account, md5Password);

                        UpdateFSEPassword(account, password);

                        bool sendSuccess = SendNewPassword(password, email);

                        if(sendSuccess)
                            MessageBox.Show("已發送密碼至註冊的信箱，請至信箱確認您的密碼。");
                        else
                            MessageBox.Show("寄信過程發生錯誤，請洽系統管理員。");
                    }
                    else
                    {
                        MessageBox.Show("您輸入的信箱錯誤");
                    }
                }
            }
        }

        private bool CheckAccount(string account)
        {
            string sqlGetAccount = $"select count(1) as [count] from tbAccounts where account_code = '{account}'";
            var accountCount = DbAdaptor.GetDataTableBySql(sqlGetAccount);

            if (accountCount.Rows[0]["count"].ToString() != "0")
            {
                return true;
            }

            return false;
        }

        private bool CheckEmail(string account, string email)
        {
            string sqlGetAccount = $"select count(1) as [count] from tbAccounts where account_code = '{account}' and user_email = '{email}'";
            var accountCount = DbAdaptor.GetDataTableBySql(sqlGetAccount);

            if (accountCount.Rows[0]["count"].ToString() != "0")
            {
                return true;
            }

            return false;
        }

        private bool SendNewPassword(string password, string email)
        {
            string body = "您好，您的新密碼為【" + password + "】。<br>" + ConfigurationManager.AppSettings["systemurl"] + "<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";
            return Mail_Send(ConfigurationManager.AppSettings["MailSender"], email , "峻富雲端物流管理系統忘記密碼通知", body, true);
        }

        private string GenerateNewPassword()
        {
            string randPwd = "";
            do
            {
                randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
            } 
            while (!Regex.IsMatch(randPwd, "\\d+[A-F]+\\d+[A-F]"));
            //具有兩個以上的英文字母，並且中間夾數字才算合格
            //將第二個英文字母變小寫
            
            int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
            randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

            return randPwd;
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static void UpdateDbPassword(string account, string md5Password)
        {
            string sqlUpdatePassword = $"update tbAccounts set password = '{md5Password}' where account_code = '{account}' ";

            DbAdaptor.ExecuteNonQuery(sqlUpdatePassword);
        }

        public static void UpdateFSEPassword(string account, string password)
        {
            var httpClient = new HttpClient() { BaseAddress = new Uri(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"]) };
            var parameters = new Dictionary<string, string> { { "UsernameAccountCode", account.Trim() }, { "Password", password } };
            string jsonStr = JsonConvert.SerializeObject(parameters);
            HttpContent contentPost = new StringContent(jsonStr, Encoding.UTF8, "application/json");
            try
            {
                //Post http callas.  
                HttpResponseMessage response = httpClient.PostAsync(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"], contentPost).Result;
                //nesekmes atveju error..
                response.EnsureSuccessStatusCode();
                //responsas to string
                string responseBody = response.Content.ReadAsStringAsync().Result;

                if (responseBody != "null")
                {
                    Console.WriteLine("True");
                }
                else
                {
                    Console.WriteLine("False");
                }
            }
            catch (HttpRequestException a)
            {
            }
        }

        public static bool Mail_Send(string MailFrom, string MailTos, string MailSub, string MailBody, bool isBodyHtml)
        {
            try
            {
                //沒給寄信人mail address
                if (string.IsNullOrEmpty(MailFrom))
                {
                    MailFrom = ConfigurationManager.AppSettings["MailSender"];
                }

                //建立MailMessage物件
                MailMessage mms = new MailMessage();
                //指定一位寄信人MailAddress
                mms.From = new MailAddress(MailFrom);
                //信件主旨
                mms.Subject = MailSub;
                //信件內容
                mms.Body = MailBody;
                //信件內容 是否採用Html格式
                mms.IsBodyHtml = isBodyHtml;

                if (MailTos != null && MailTos.Length > 0)//防呆
                {
                     mms.To.Add(new MailAddress(MailTos.Trim()));
                }

                using (SmtpClient client = new SmtpClient())//或公司、客戶的smtp_server
                {
                    client.Host = "msa.hinet.net";
                    client.Port = 25;
                    client.Send(mms);//寄出一封信
                }//end using 

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
