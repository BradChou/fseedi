﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt;

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = File.Open("E:/testGrid.xlsx", FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                {
                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });

                    dt = result.Tables[0];
                }
            }

            //dataGridView1.DataSource = dt;

            dataGridView1.Width = dt.Columns.Count * 100;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dataGridView1.Columns.Add(new SpecialColumnHeader());
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var r = dt.Rows[i];
                dataGridView1.Rows.Add(r.ItemArray);
            }

            //foreach (DataGridViewColumn col in dataGridView1.Columns)
            //{
            //    col.Frozen = true;
            //}

            ////dataGridView1.Rows[0].Frozen = true;

            //dataGridView1.ScrollBars = ScrollBars.Both;
        }
    }
}
