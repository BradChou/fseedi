using System.Windows.Forms;

public class SpecialColumnHeader : DataGridViewTextBoxColumn
{
    ComboNumericHeaderCell headerCell = new ComboNumericHeaderCell();

    public SpecialColumnHeader()
    {
        //this.CellTemplate = new DataGridViewTextBoxCell();
        this.HeaderCell = headerCell;
        this.Width = 150;
    }

    protected override void OnDataGridViewChanged()
    {
        if (DataGridView != null)
        {
            this.DataGridView.Controls.Add(headerCell._comboBox);
        }
    }        
}