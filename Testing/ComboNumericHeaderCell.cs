using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

public class ComboNumericHeaderCell : DataGridViewColumnHeaderCell
{
    public readonly ComboBox _comboBox;

    public ComboNumericHeaderCell()
    {
        _comboBox = new ComboBox();
        _comboBox.Font = Control.DefaultFont;

        _comboBox.MouseClick += new MouseEventHandler(MouseClick);
        // TODO - Wireup necessary events
    }

    private void MouseClick(object sender, MouseEventArgs e)
    {
        Debug.WriteLine("x:{0}, y:{1}, button:{2}", e.X, e.Y, e.Button);
        OnClick(new DataGridViewCellEventArgs(ColumnIndex, RowIndex));
    }

    public string ComboBoxValue
    {
        get { return _comboBox.SelectedValue.ToString(); }
        set { _comboBox.SelectedText = value; }
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates dataGridViewElementState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
        base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);

        _comboBox.Location = new Point(cellBounds.Left, cellBounds.Top);
        _comboBox.Height = cellBounds.Height;
        _comboBox.Width = cellBounds.Width;

        _comboBox.Height = cellBounds.Height;
        //_comboBox.Width = FixedWidth;
    }

}